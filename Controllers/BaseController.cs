﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json;

namespace Admin.Controllers
{
    public class BaseController : Controller
    {
        public static User_Model UserLog = new User_Model();
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
            var session = HttpContext.Session;
            var key = "UserLogged";
            var UserModel = session.GetString(key);

            if (UserModel == null || 
                UserModel == string.Empty)
            {                
                string url = Url.Action("SignIn", "Auth");
                filterContext.Result = new RedirectResult(url);
                return;
            }
            else
            {
                UserLog = JsonConvert.DeserializeObject<User_Model>(UserModel);
            }
        }
    }
}