﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Admin.Controllers
{
    public class AuthController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult SignIn()
        {
            ViewBag.Message = "NotAuthen";
            return View("~/Views/Shared/Auth/SignIn.cshtml");
        }
        [HttpPost]
        public IActionResult SignIn(string UserName, string Password)
        {
            User_Info zInfo = new User_Info(UserName, Password);
            if (zInfo.Code == "200")
            {
                ViewBag.Message = "IsAuthen";

                // Save
                var key = "UserLogged";
                var obj = JsonConvert.SerializeObject(zInfo.User);
                HttpContext.Session.SetString(key, obj);

                string url = Url.Action("Index", "Home");
                Redirect(url);
            }
            else
            {
                ViewBag.Message = "405";
            }

            return View("~/Views/Shared/Auth/SignIn.cshtml");
        }

        public IActionResult SignOut()
        {
            HttpContext.Session.Clear();
            return View("~/Views/Shared/Auth/SignIn.cshtml");
        }
    }
}