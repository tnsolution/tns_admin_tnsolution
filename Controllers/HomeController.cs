﻿using Admin.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;

namespace Admin.Controllers
{
    public class HomeController : BaseController
    {
        public IActionResult Index()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error(string Message = "")
        {
            ViewBag.Message = Message;
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        //-----------------------------------------------------------------------------------------------------------
        #region [--PARTNER--]
        public IActionResult Partner()
        {
            ViewBag.ListData = Partner_Data.List();
            return View("~/Views/Home/Partner.cshtml");
        }

        public IActionResult Edit_Partner(string PartnerNumber = "")
        {
            Partner_Info zPartnerInfo = new Partner_Info(PartnerNumber);
            return View("~/Views/Home/Partial/Edit_Partner.cshtml", zPartnerInfo.Partner);
        }

        public IActionResult Save_Partner(
           string PartnerNumber, string PartnerName, string PartnerID,
           string PartnerPhone, string PartnerAddress,
           string Base64String = "", bool Activated = false,
           string StoreID = "", string StoreName = "")
        {
            string Key = PartnerNumber == null ? "" : PartnerNumber;
            var zResult = new ServerResult();

            #region [Info]
            Partner_Model zModel = new Partner_Model();
            zModel.Logo_Large = Base64String == null ? "" : Base64String;
            zModel.PartnerNumber = Key;
            zModel.PartnerID = PartnerID.Trim();
            zModel.PartnerName = PartnerName.Trim();
            zModel.PartnerAddress = PartnerAddress.Trim();
            zModel.PartnerPhone = PartnerPhone.Trim();

            zModel.Activated = Activated;
            if (Activated)
            {
                zModel.ActivationDate = DateTime.Now;
            }

            zModel.StoreID = StoreID.Trim();
            zModel.StoreName = StoreName.Trim();

            Partner_Info zInfo = new Partner_Info();
            if (Key.Length >= 36)
            {
                zModel.ClearNullable();
                zInfo.Partner = zModel;
                zInfo.Update();
            }
            else
            {
                Key = Guid.NewGuid().ToString();
                zModel.PartnerNumber = Key;
                zModel.ClearNullable();
                zInfo.Partner = zModel;
                zInfo.Create_ClientKey();
            }
            #endregion

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Message = "Đã cập nhật thành công !.";
                zResult.Success = true;
            }
            else
            {
                zResult.Message = zInfo.Message.GetFirstLine();
                zResult.Success = false;
            }

            return RedirectToAction("Partner");
        }

        public JsonResult Delete_Partner(string PartnerNumber = "")
        {
            var zResult = new ServerResult();
            if (PartnerNumber.Length > 0)
            {
                Partner_Info zInfo = new Partner_Info(PartnerNumber);
                zInfo.Delete();

                if (zInfo.Code == "200" ||
                    zInfo.Code == "201")
                {
                    zResult.Message = "Đã xóa thành công !.";
                    zResult.Success = true;
                }
                else
                {
                    zResult.Message = zInfo.Message.GetFirstLine();
                    zResult.Success = false;
                }
            }
            else
            {
                zResult.Message = "Không tìm thấy thông tin !.";
                zResult.Success = false;
            }

            return Json(zResult);
        }
        #endregion

        //-----------------------------------------------------------------------------------------------------------
        #region [--ROLE--] 
        public IActionResult Role(string Module = "", int Slug = 0)
        {
            string Message = "";
            ViewBag.ListModule = Authen.GetModule(out Message);
            ViewBag.ListRole = Authen.Recursive_Role(Module, Slug);
            return View("~/Views/Home/Role.cshtml");
        }
        public IActionResult Edit_Role(string RoleKey = "", string Module = "")
        {
            string Message = "";
            ViewBag.ListModule = Authen.GetModule(out Message);
            ViewBag.ListRole = Authen.Recursive_Role(Module);

            var zInfo = new Role_Info(RoleKey);
            return PartialView("~/Views/Home/Partial/Edit_Role.cshtml", zInfo.Role);
        }

        public IActionResult Update_Role(
            string RoleKey = "", string Module = "", string Level = "0", string RoleName = "",
            string Parent = "0", string Description = "", string RouteName = "", string Icon = "",
            string RoleID = "", string ControllerName = "", string ActionName = "", string ParamName = "",
            int Rank = 0, int Slug = 0)
        {
            var zResult = new ServerResult();
            RoleKey ??= "";

            #region [--Save Data--]
            var zInfo = new Role_Info();
            zInfo.Role.RoleKey = RoleKey;
            zInfo.Role.Level = Level.ToInt();
            zInfo.Role.RoleName = RoleName;
            zInfo.Role.RouteName = RouteName;
            zInfo.Role.Parent = Parent;
            zInfo.Role.Icon = Icon;
            zInfo.Role.RoleID = RoleID;
            zInfo.Role.Rank = Rank;
            zInfo.Role.ControllerName = ControllerName;
            zInfo.Role.ParamName = ParamName;
            zInfo.Role.ActionName = ActionName;
            zInfo.Role.Module = Module;
            zInfo.Role.Description = Description;
            zInfo.Role.Slug = Slug; //9 là di động, 8 là mobile

            zInfo.Role.ClearNullable();
            if (RoleKey.Length >= 36)
            {
                zInfo.Update();
            }
            else
            {
                zInfo.Create_ServerKey();
            }

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Message = "Cập nhật thành công !.";
                zResult.Success = true;
            }
            else
            {
                zResult.Message = zInfo.Message.GetFirstLine();
                zResult.Success = false;
            }
            #endregion

            return RedirectToAction("Role", new { Module });
        }

        public JsonResult Copy_Role(string RoleKey = "")
        {
            var zResult = new ServerResult();
            if (RoleKey.Length > 0)
            {
                var zInfo = new Role_Info(RoleKey);
                zInfo.Copy();

                if (zInfo.Code == "200" ||
                    zInfo.Code == "201")
                {
                    zResult.Data = zInfo.Role.RoleKey;//JsonConvert.SerializeObject(zInfo.Role);
                    zResult.Message = "Đã copy thành công !.";
                    zResult.Success = true;
                }
                else
                {
                    zResult.Message = zInfo.Message.GetFirstLine();
                    zResult.Success = false;
                }
            }
            else
            {
                zResult.Message = "Không tìm thấy thông tin !.";
                zResult.Success = false;
            }

            return Json(zResult);
        }

        public JsonResult Delete_Role(string RoleKey = "")
        {
            var zResult = new ServerResult();
            if (RoleKey.Length > 0)
            {
                var zInfo = new Role_Info(RoleKey);
                zInfo.Delete();

                if (zInfo.Code == "200" ||
                    zInfo.Code == "201")
                {
                    zResult.Message = "Đã xóa thành công !.";
                    zResult.Success = true;
                }
                else
                {
                    zResult.Message = zInfo.Message.GetFirstLine();
                    zResult.Success = false;
                }
            }
            else
            {
                zResult.Message = "Không tìm thấy thông tin !.";
                zResult.Success = false;
            }

            return Json(zResult);
        }
        #endregion

        //-----------------------------------------------------------------------------------------------------------
        #region [--ROLE PARTNER--]
        public IActionResult Role_Partner()
        {
            ViewBag.ListModule = Authen.GetModule(out string Message);
            ViewBag.ListPartner = Partner_Data.List();

            return View();
        }
        public IActionResult Role_Data_Partner(string PartnerNumber = "", string Module = "")
        {
            if (string.IsNullOrEmpty(Module))
            {
                Module = "";
            }
            ViewBag.ListDataPartner = Authen.GetPartnerRole(PartnerNumber, Module, out string Message);
            return View("~/Views/Home/Partial/Role_Data_Partner.cshtml");
        }
        public IActionResult Role_Data_Default(string PartnerNumber = "", string Module = "")
        {
            if (string.IsNullOrEmpty(Module))
            {
                Module = "";
            }
            ViewBag.ListDataRole = Authen.Recursive_Role(PartnerNumber, Module);
            return View("~/Views/Home/Partial/Role_Data_Default.cshtml");
        }

        public JsonResult Role_Remove(string PartnerNumber = "", string RoleKey = "")
        {
            var zResult = new ServerResult();
            var zInfo = new Role_Partner_Info();
            zInfo.Delete(PartnerNumber, RoleKey);

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Message = "Đã xóa thành công !.";
                zResult.Success = true;
            }
            else
            {
                zResult.Message = zInfo.Message.GetFirstLine();
                zResult.Success = false;
            }
            return Json(zResult);
        }
        public JsonResult Role_Add(string PartnerNumber = "", string RoleKey = "", string Module = "")
        {
            var zResult = new ServerResult();
            var zInfo = new Role_Partner_Info();
            zInfo.Create(PartnerNumber, RoleKey, Module);

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Message = "Đã thêm thành công !.";
                zResult.Success = true;
            }
            else
            {
                zResult.Message = zInfo.Message.GetFirstLine();
                zResult.Success = false;
            }
            return Json(zResult);
        }
        public JsonResult Role_Add_All(List<Role_Partner_Model> ListRole)
        {
            var zResult = new ServerResult();
            if (ListRole.Count > 0)
            {
                string SQL = "";
                foreach (var item in ListRole)
                {
                    SQL += @"INSERT INTO SYS_Role_Partner (PartnerNumber, RoleKey , Module ) VALUES ('" + item.PartnerNumber + "', '" + item.RoleKey + "' , N'" + item.Module + "')" + Environment.NewLine;
                }

                string Message = "";
                if (SQL != string.Empty)
                {
                    Authen.ApplySQL(SQL, out Message);
                }

                if (Message == string.Empty)
                {
                    zResult.Message = "Đã cập nhật thành công !.";
                    zResult.Success = true;
                }
                else
                {
                    zResult.Message = Message;
                    zResult.Success = false;
                }
            }
            else
            {
                zResult.Message = "Không có dữ liệu";
                zResult.Success = false;
            }
            return Json(zResult);
        }
        public JsonResult Role_Remove_All(string PartnerNumber = "", string Module = "")
        {
            var zResult = new ServerResult();
            var zInfo = new Role_Partner_Info();
            zInfo.Delete(PartnerNumber);
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Message = "Đã xóa thành công !.";
                zResult.Success = true;
            }
            else
            {
                zResult.Message = zInfo.Message.GetFirstLine();
                zResult.Success = false;
            }
            return Json(zResult);
        }
        #endregion

        //-----------------------------------------------------------------------------------------------------------
        #region [--ACCOUNT--]
        public IActionResult Account()
        {
            ViewBag.ListPartner = Partner_Data.List();
            return View();
        }
        public IActionResult Account_Partner(string PartnerNumber = "")
        {
            PartnerNumber = PartnerNumber == null ? "" : PartnerNumber;
            List<User_Model> zList = User_Data.List(PartnerNumber);
            return View("~/Views/Home/Partial/Table_Account_Data.cshtml", zList);
        }
        public IActionResult Edit_Account(string UserKey)
        {
            var zResult = new ServerResult();
            var zInfo = new User_Info(UserKey);
            ViewBag.ListPartner = Partner_Data.List();
            return View("~/Views/Home/Partial/Edit_Account.cshtml", zInfo.User);
        }
        public IActionResult Save_Account(
    string UserKey = "", string UserName = "", string Password = "",
    string Description = "", string Activate = "",
    string ExpireDate = "", string PartnerNumber = "")
        {
            UserKey = UserKey == null ? "" : UserKey;

            var zResult = new ServerResult();
            var zInfo = new User_Info(UserKey);
            zInfo.User.PartnerNumber = PartnerNumber;
            zInfo.User.UserName = UserName.Trim();
            zInfo.User.Description = Description.Trim();

            if (UserKey != string.Empty)
            {
                //kiểm tra cập nhật mật khẩu
                if (zInfo.User.Password != Password.Trim())
                {
                    zInfo.User.Password = TN_Utils.HashPass(Password.Trim());
                }
                else
                {
                    zInfo.User.Password = Password.Trim();
                }
            }
            else
            {
                zInfo.User.Password = TN_Utils.HashPass(Password.Trim());
            }

            DateTime zExpireDate = DateTime.Now.AddYears(1);
            if (ExpireDate != string.Empty)
            {
                DateTime.TryParseExact(ExpireDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zExpireDate);
            }

            zInfo.User.ExpireDate = zExpireDate;
            if (Activate.ToInt() == 0)
            {
                zInfo.User.Activate = false;
            }
            else
            {
                zInfo.User.Activate = true;
            }

            zInfo.User.CreatedBy = UserLog.UserKey;
            zInfo.User.CreatedName = UserLog.EmployeeName;
            zInfo.User.ModifiedBy = UserLog.UserKey;
            zInfo.User.ModifiedName = UserLog.EmployeeName;

            if (UserKey.Length < 36)
            {
                zInfo.User.UserKey = Guid.NewGuid().ToString();
                zInfo.User.ClearNullable();
                zInfo.Create_ClientKey();
            }
            else
            {
                zInfo.User.ClearNullable();
                zInfo.Update();
            }

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                zResult.Message = "Cập nhật thành công !.";
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message.GetFirstLine();
            }


            if (zResult.Success)
            {
                return RedirectToAction("Account", new { PartnerNumber });
            }
            else
            {
                return RedirectToAction("Error", new { zResult.Message });
            }
        }
        public JsonResult Check_Account(string UserName)
        {
            var zResult = new ServerResult();
            int zCount = User_Data.CheckUserName(UserName);

            if (zCount > 0)
            {
                zResult.Success = false;
                zResult.Message = "Tên đăng nhập này đã tồn tại trong dữ liệu vui lòng chọn tên khác !.";
            }
            else
            {
                zResult.Success = true;
            }

            return Json(zResult);
        }
        public JsonResult Reset_Pass(string UserKey)
        {
            var zResult = new ServerResult();
            string Random = TN_Utils.RandomPassword();
            var zInfo = new User_Info();
            zInfo.User.ModifiedName = UserLog.UserName;
            zInfo.User.ModifiedBy = UserLog.UserKey;
            zInfo.ResetPass(UserKey, TN_Utils.HashPass(Random));

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = Random;
                zResult.Message = "Mật khẩu mới của " + zInfo.User.UserName + " là: " + Random;
            }
            else
            {
                zResult.Success = false;
                zResult.Data = string.Empty;
                zResult.Message = zInfo.Message;
            }

            return Json(zResult);
        }
        public JsonResult Activate(string UserKey)
        {
            var zResult = new ServerResult();

            var zInfo = new User_Info();
            zInfo.User.ModifiedName = UserLog.EmployeeName;
            zInfo.User.ModifiedBy = UserLog.UserKey;
            zInfo.SetActivate(UserKey);

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = string.Empty;
                zResult.Message = string.Empty;
            }
            else
            {
                zResult.Success = false;
                zResult.Data = string.Empty;
                zResult.Message = zInfo.Message;
            }

            return Json(zResult);
        }
        public JsonResult Change_Pass(string OldPass, string NewPass, string PartnerNumber)
        {
            var zResult = new ServerResult();
            var zInfo = new User_Info(UserLog.UserName, OldPass, PartnerNumber);
            if (zInfo.Code != "200")
            {
                zResult.Success = false;
                zResult.Message = "Tên mật khẩu không đúng vui lòng thử lại !.";
                return Json(zResult);
            }

            zInfo.User.ModifiedName = UserLog.EmployeeName;
            zInfo.User.ModifiedBy = UserLog.UserKey;
            zInfo.ResetPass(UserLog.UserKey, TN_Utils.HashPass(NewPass));

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = NewPass;
                zResult.Message = "Mật khẩu mới của " + zInfo.User.UserName + " là: " + NewPass;
            }
            else
            {
                zResult.Success = false;
                zResult.Data = string.Empty;
                zResult.Message = zInfo.Message;
            }

            return Json(zResult);
        }
        public JsonResult Delete_Account(string UserKey)
        {
            var zResult = new ServerResult();
            var zInfo = new User_Info();
            zInfo.Delete(UserKey);
            User_Model zModel = zInfo.User;
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message;
                return Json(zResult);
            }
        }
        #endregion

        //-----------------------------------------------------------------------------------------------------------
        #region [--ACCOUNT ROLE--]
        public IActionResult Account_Role(string PartnerNumber, string UserKey, int Slug)
        {
            var zInfo = new User_Info(UserKey);
            if (Slug == 9)
            {
                ViewBag.TitleHead = "Phân quyền truy cập chức năng di động cho tài khoản " + zInfo.User.UserName;
            }
            else
            {
                ViewBag.TitleHead = "Phân quyền truy cập chức năng để bàn cho tài khoản " + zInfo.User.UserName;
            }
            ViewBag.ListRolePartner = Authen.GetPartnerRole(PartnerNumber, UserKey, Slug, out string Message);
            ViewBag.ListRoleUser = Authen.GetUserRole(PartnerNumber, UserKey, Slug, out Message);

            return View("~/Views/Home/Partial/Edit_Account_Role.cshtml", zInfo.User);
        }
        public IActionResult Role_Account_Set(string PartnerNumber, string UserKey, int Slug)
        {
            ViewBag.ListData = Authen.GetUserRole(PartnerNumber, UserKey, Slug, out string Message);
            return View("~/Views/Home/Partial/Role_Account_Set.cshtml");
        }
        public IActionResult Role_Account_Default(string PartnerNumber, string UserKey, int Slug)
        {
            ViewBag.ListData = Authen.GetPartnerRole(PartnerNumber, UserKey, Slug, out string Message);
            return View("~/Views/Home/Partial/Role_Account_Default.cshtml");
        }

        public JsonResult Role_User_Add(string UserKey, string RoleKey, int Slug)
        {
            var zResult = new ServerResult();
            var zInfo = new User_Info();
            zInfo.Add_Role(UserKey, RoleKey, Slug);

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Message = "Đã cập nhật thành công !.";
                zResult.Success = true;
            }
            else
            {
                zResult.Message = zInfo.Message.GetFirstLine();
                zResult.Success = false;
            }
            return Json(zResult);
        }
        public JsonResult Role_User_Add_All(List<User_Role> ListRole)
        {
            var zResult = new ServerResult();
            if (ListRole.Count > 0)
            {
                string SQL = "";
                foreach (var item in ListRole)
                {
                    SQL += @"INSERT INTO SYS_User_Role (PartnerNumber, RoleKey, UserKey) VALUES ('" + item.PartnerNumber + "', '" + Guid.Parse(item.RoleKey) + "' , '" + Guid.Parse(item.UserKey) + "')" + Environment.NewLine;
                }

                string Message = "";
                if (SQL != string.Empty)
                {
                    Authen.ApplySQL(SQL, out Message);
                }

                if (Message == string.Empty)
                {
                    zResult.Message = "Đã cập nhật thành công !.";
                    zResult.Success = true;
                }
                else
                {
                    zResult.Message = Message;
                    zResult.Success = false;
                }
            }
            else
            {
                zResult.Message = "Không có dữ liệu";
                zResult.Success = false;
            }
            return Json(zResult);
        }
        public JsonResult Role_User_Remove(string UserKey, string RoleKey)
        {
            var zResult = new ServerResult();
            var zInfo = new User_Info();
            zInfo.Delete_Role(UserKey, RoleKey);

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Message = "Đã xóa thành công !.";
                zResult.Success = true;
            }
            else
            {
                zResult.Message = zInfo.Message.GetFirstLine();
                zResult.Success = false;
            }
            return Json(zResult);
        }
        public JsonResult Role_User_Remove_All(string UserKey, int Slug)
        {
            var zResult = new ServerResult();
            var zInfo = new User_Info();
            zInfo.Delete_Role(UserKey);

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Message = "Đã xóa thành công !.";
                zResult.Success = true;
            }
            else
            {
                zResult.Message = zInfo.Message.GetFirstLine();
                zResult.Success = false;
            }
            return Json(zResult);
        }
        public JsonResult Role_User_Save(string UserKey, string Role, int Slug)
        {
            var zResult = new ServerResult();
            var zInfo = new User_Info(UserKey);
            var zUser = zInfo.User;

            if (Role.Length > 0)
            {
                var zRole = JsonConvert.DeserializeObject<List<User_Role>>(Role);
                foreach (User_Role s in zRole)
                {
                    zInfo.Edit_Role(UserKey, s.RoleKey, s.RoleRead, s.RoleAdd, s.RoleEdit, s.RoleDel, s.RoleExe, Slug);
                    if (zInfo.Code == "500")
                    {
                        break;
                    }
                }

                if (zInfo.Code == "500")
                {
                    zResult.Success = false;
                    zResult.Message = zInfo.Message.GetFirstLine();
                }
                else
                {
                    zResult.Success = true;
                    zResult.Message = "";
                }
            }

            return Json(zResult);
        }
        #endregion

        //-----------------------------------------------------------------------------------------------------------
        #region [GROUP_ROLES]
        public IActionResult Group()
        {
            ViewBag.ListGroup = Group_Data.List(UserLog.PartnerNumber, out string Message);
            return View("~/Views/Setting/Group.cshtml");
        }
        public JsonResult Save_Group(string GroupKey = "", string GroupName = "", string Description = "")
        {
            var zResult = new ServerResult();
            var zInfo = new Group_Info();
            var zModel = new Group_Model
            {
                PartnerNumber = UserLog.PartnerNumber,
                GroupKey = GroupKey,
                GroupName = GroupName.Trim(),
                Description = Description.Trim(),
                CreatedBy = UserLog.UserKey,
                CreatedName = UserLog.UserName,
                ModifiedBy = UserLog.UserKey,
                ModifiedName = UserLog.UserName
            };

            zModel.ClearNullable();
            if (string.IsNullOrEmpty(GroupKey))
            {
                zInfo.Group = zModel;
                zInfo.Create_ServerKey();
            }
            else
            {
                zInfo.Group = zModel;
                zInfo.Update();
            }

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message.GetFirstLine();
                return Json(zResult);
            }
        }
        public JsonResult Edit_Group(string GroupKey)
        {
            var zResult = new ServerResult();
            var zInfo = new Group_Info(GroupKey);
            var zModel = zInfo.Group;
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zModel);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message.GetFirstLine();
            }
            return Json(zResult);
        }
        public JsonResult Delete_Group(string GroupKey)
        {
            var zResult = new ServerResult();
            var zInfo = new Group_Info();
            zInfo.Group.GroupKey = GroupKey;
            zInfo.Delete();

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                return Json(zResult);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message.GetFirstLine();
                return Json(zResult);
            }
        }
        #endregion
    }
}
