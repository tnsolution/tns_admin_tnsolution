﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

$(document).ready(function () {
    $(".se-pre-con").fadeOut("slow");

    $("#sidebar-left").on("click", "a", function () {
        var link = $(this).attr("href");
        if (link !== "#") {
            $(".se-pre-con").fadeIn("slow");
            localStorage.setItem("url", link);
        }
    });
    $(".select2").select2({
        width: "100%",
        placeholder: "--Chọn--",
        tags: true,
        createTag: function (params) {
            return {
                id: params.term,
                text: params.term,
                newOption: true
            };
        }
    });
    $(".datepicker").datepicker({
        todayHighlight: true,
        autoclose: true,
        format: "dd/mm/yyyy"
    });
    $("div[data-plugin-datepicker]").datepicker({
        todayHighlight: true,
        format: "dd/mm/yyyy",
    }).on("changeDate", function (e) {
        var date = $(this).datepicker("getFormattedDate");
        var attr = $(this).attr("name");
        $("#txt_" + attr).val(date);
    });

    //$("td.number").number(true, 0, ",", ".");
    //$("input.number").number(true, 0, ",", ".");
    //$("span.number").number(true, 0, ",", ".");
    //$("del.number").number(true, 0, ",", ".");

    $(".number").each(function () {
        $(this).number($(this).text(), 0, ',', '.');
    });

    if (localStorage.getItem("url") !== null) {
        var url = localStorage.getItem("url");
        $("#sidebar-left a[href=\"" + url + "\"]").addClass("active")
            .closest("li")
            .parents("li.nav-parent")
            .addClass("nav-expanded");
    }
    else {
        var pathname = document.location.pathname;
        $("#sidebar-left a").each(function () {
            var value = jQuery(this).attr("href");
            if (pathname.indexOf(value) > -1) {
                $(this).addClass("active")
                    .closest("li")
                    .parents("li.nav-parent")
                    .addClass("nav-expanded");
                return false;
            }
        });
    }
});
$(document).on("click", "a[viewdoc]", function (e) {
    var link = "";
    var url = "";
    if (location.port.length > 0) {
        url = document.location.protocol + "//" + document.location.hostname + ":" + location.port;
    }
    else {
        url = document.location.protocol + "//" + document.location.hostname;
    }

    var attr = $(this).attr("href");
    if (attr.indexOf(".doc") !== -1 ||
        attr.indexOf(".docx") !== -1) {
        link += "http://docs.google.com/viewer?embedded=true&url=" + url + $(this).attr("href");
        return;
    }
    if (attr.indexOf(".xls") !== -1 ||
        attr.indexOf(".xlsx") !== -1) {
        link += "http://docs.google.com/viewer?embedded=true&url=" + url + $(this).attr("href");
        return;
    }
    if (attr.indexOf(".pdf") !== -1) {
        link += "http://docs.google.com/viewer?embedded=true&url=" + url + $(this).attr("href");
    }

    var height = $(window).height();
    var iframe = "<iframe name = \"iFrameBackOffice\" frameborder = \"0\" marginwidth = \"0\" marginheight = \"0\" allowfullscreen scrolling = \"no\" style = \"position: relative; border: none; padding: 0px; margin: 0px;\" width = \"100%\" height = \"" + height + "\" src = \"" + link + "\"></iframe>";

    $.dialog({
        theme: "supervan",
        closeIcon: true,
        columnClass: "xlarge",
        title: "Xem tập tin",
        content: iframe,
        animation: "scale",
        closeAnimation: "scale",
        backgroundDismiss: true,
    });
});

$(document).on("click", ".modal-dismiss", function (e) {
    e.preventDefault();
    $.magnificPopup.close();
});
$(document).on("click", ".modal-confirm", function (e) {
    if ($(this).attr("type") === "submit") {
        return;
    }
    e.preventDefault();
    $.magnificPopup.close();
});