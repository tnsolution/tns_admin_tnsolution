﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
namespace Admin
{
    public class Group_Data
    {
        public static List<Group_Model> List(string PartnerNumber, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM SYS_Group WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ";
            string zConnectionString = ConfigurationManager.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Group_Model> zList = new List<Group_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Group_Model()
                {
                    GroupKey = r["GroupKey"].ToString(),
                    GroupName = r["GroupName"].ToString(),
                    Description = r["Description"].ToString(),
                    IsPublish = r["IsPublish"].ToBool(),
                    Module = r["Module"].ToString(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }
    }
}
