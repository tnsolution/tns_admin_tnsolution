﻿using System;
using System.Data;
using System.Data.SqlClient;
namespace Admin
{
    public class Group_Info
    {

        public Group_Model Group = new Group_Model();
        private string _Message = "";
        public string Code
        {
            get
            {
                if (_Message.Length >= 3)
                {
                    return _Message.Substring(0, 3);
                }
                else
                {
                    return "";
                }
            }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        #region [ Constructor Get Information ]
        public Group_Info()
        {
            Group.GroupKey = Guid.NewGuid().ToString();
        }
        public Group_Info(string GroupKey)
        {
            string zSQL = "SELECT * FROM SYS_Group WHERE GroupKey = @GroupKey AND RecordStatus != 99 ";
            string zConnectionString = ConfigurationManager.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@GroupKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(GroupKey);
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    Group.GroupKey = zReader["GroupKey"].ToString();
                    Group.GroupName = zReader["GroupName"].ToString();
                    Group.Description = zReader["Description"].ToString();
                    if (zReader["IsPublish"] != DBNull.Value)
                    {
                        Group.IsPublish = (bool)zReader["IsPublish"];
                    }

                    Group.Module = zReader["Module"].ToString();
                    Group.PartnerNumber = zReader["PartnerNumber"].ToString();
                    if (zReader["RecordStatus"] != DBNull.Value)
                    {
                        Group.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    }

                    if (zReader["CreatedOn"] != DBNull.Value)
                    {
                        Group.CreatedOn = (DateTime)zReader["CreatedOn"];
                    }

                    Group.CreatedBy = zReader["CreatedBy"].ToString();
                    Group.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                    {
                        Group.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    }

                    Group.ModifiedBy = zReader["ModifiedBy"].ToString();
                    Group.ModifiedName = zReader["ModifiedName"].ToString();
                    _Message = "200 OK";
                }
                else
                {
                    _Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }

        #endregion

        #region [ Constructor Update Information ]

        public string Create_ServerKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO SYS_Group ("
         + " GroupName , Description , IsPublish , Module , PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @GroupName , @Description , @IsPublish , @Module , @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@GroupName", SqlDbType.NVarChar).Value = Group.GroupName;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Group.Description;
                if (Group.IsPublish == null)
                {
                    zCommand.Parameters.Add("@IsPublish", SqlDbType.Bit).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@IsPublish", SqlDbType.Bit).Value = Group.IsPublish;
                }

                zCommand.Parameters.Add("@Module", SqlDbType.NVarChar).Value = Group.Module;
                if (Group.PartnerNumber != "" && Group.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Group.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Group.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Group.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Group.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Group.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Group.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Create_ClientKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO SYS_Group("
         + " GroupKey , GroupName , Description , IsPublish , Module , PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @GroupKey , @GroupName , @Description , @IsPublish , @Module , @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (Group.GroupKey != "" && Group.GroupKey.Length == 36)
                {
                    zCommand.Parameters.Add("@GroupKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Group.GroupKey);
                }
                else
                {
                    zCommand.Parameters.Add("@GroupKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@GroupName", SqlDbType.NVarChar).Value = Group.GroupName;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Group.Description;
                if (Group.IsPublish == null)
                {
                    zCommand.Parameters.Add("@IsPublish", SqlDbType.Bit).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@IsPublish", SqlDbType.Bit).Value = Group.IsPublish;
                }

                zCommand.Parameters.Add("@Module", SqlDbType.NVarChar).Value = Group.Module;
                if (Group.PartnerNumber != "" && Group.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Group.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Group.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Group.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Group.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Group.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Group.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Update()
        {
            string zSQL = "UPDATE SYS_Group SET "
                        + " GroupName = @GroupName,"
                        + " Description = @Description,"
                        + " IsPublish = @IsPublish,"
                        + " Module = @Module,"
                        + " PartnerNumber = @PartnerNumber,"
                        + " RecordStatus = @RecordStatus,"
                        + " ModifiedOn = GetDate(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                        + " WHERE GroupKey = @GroupKey";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (Group.GroupKey != "" && Group.GroupKey.Length == 36)
                {
                    zCommand.Parameters.Add("@GroupKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Group.GroupKey);
                }
                else
                {
                    zCommand.Parameters.Add("@GroupKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@GroupName", SqlDbType.NVarChar).Value = Group.GroupName;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Group.Description;
                if (Group.IsPublish == null)
                {
                    zCommand.Parameters.Add("@IsPublish", SqlDbType.Bit).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@IsPublish", SqlDbType.Bit).Value = Group.IsPublish;
                }

                zCommand.Parameters.Add("@Module", SqlDbType.NVarChar).Value = Group.Module;
                if (Group.PartnerNumber != "" && Group.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Group.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Group.RecordStatus;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Group.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Group.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE SYS_Group SET RecordStatus = 99 WHERE GroupKey = @GroupKey";
            string zConnectionString = ConfigurationManager.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@GroupKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Group.GroupKey);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Empty()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM SYS_Group WHERE GroupKey = @GroupKey";
            string zConnectionString = ConfigurationManager.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@GroupKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Group.GroupKey);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
