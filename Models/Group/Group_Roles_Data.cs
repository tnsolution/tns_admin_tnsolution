﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
namespace Admin
{
    public class Group_Roles_Data
    {
        public static List<Group_Roles_Model> List(string PartnerNumber, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM SYS_Group_Roles WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ";
            string zConnectionString = ConfigurationManager.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Group_Roles_Model> zList = new List<Group_Roles_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Group_Roles_Model()
                {
                    AutoKey = r["AutoKey"].ToInt(),
                    GroupKey = r["GroupKey"].ToString(),
                    RoleKey = r["RoleKey"].ToString(),
                    RoleRead = r["RoleRead"].ToBool(),
                    RoleAdd = r["RoleAdd"].ToBool(),
                    RoleEdit = r["RoleEdit"].ToBool(),
                    RoleDel = r["RoleDel"].ToBool(),
                    RoleApprove = r["RoleApprove"].ToBool(),
                    RoleName = r["RoleName"].ToString(),
                    RoleID = r["RoleID"].ToString(),
                    RoleURL = r["RoleURL"].ToString(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }
    }
}
