﻿using System;
using System.Data;
using System.Data.SqlClient;
namespace Admin
{
    public class Group_Roles_Info
    {

        public Group_Roles_Model Group_Roles = new Group_Roles_Model();
        private string _Message = "";
        public string Code
        {
            get
            {
                if (_Message.Length >= 3)
                {
                    return _Message.Substring(0, 3);
                }
                else
                {
                    return "";
                }
            }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        #region [ Constructor Get Information ]
        public Group_Roles_Info()
        {
        }
        public Group_Roles_Info(int AutoKey)
        {
            string zSQL = "SELECT * FROM SYS_Group_Roles WHERE AutoKey = @AutoKey AND RecordStatus != 99 ";
            string zConnectionString = ConfigurationManager.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                    {
                        Group_Roles.AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    }

                    Group_Roles.GroupKey = zReader["GroupKey"].ToString();
                    Group_Roles.RoleKey = zReader["RoleKey"].ToString();
                    if (zReader["RoleRead"] != DBNull.Value)
                    {
                        Group_Roles.RoleRead = (bool)zReader["RoleRead"];
                    }

                    if (zReader["RoleAdd"] != DBNull.Value)
                    {
                        Group_Roles.RoleAdd = (bool)zReader["RoleAdd"];
                    }

                    if (zReader["RoleEdit"] != DBNull.Value)
                    {
                        Group_Roles.RoleEdit = (bool)zReader["RoleEdit"];
                    }

                    if (zReader["RoleDel"] != DBNull.Value)
                    {
                        Group_Roles.RoleDel = (bool)zReader["RoleDel"];
                    }

                    if (zReader["RoleApprove"] != DBNull.Value)
                    {
                        Group_Roles.RoleApprove = (bool)zReader["RoleApprove"];
                    }

                    Group_Roles.RoleName = zReader["RoleName"].ToString();
                    Group_Roles.RoleID = zReader["RoleID"].ToString();
                    Group_Roles.RoleURL = zReader["RoleURL"].ToString();
                    if (zReader["RecordStatus"] != DBNull.Value)
                    {
                        Group_Roles.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    }

                    if (zReader["CreatedOn"] != DBNull.Value)
                    {
                        Group_Roles.CreatedOn = (DateTime)zReader["CreatedOn"];
                    }

                    Group_Roles.CreatedBy = zReader["CreatedBy"].ToString();
                    Group_Roles.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                    {
                        Group_Roles.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    }

                    Group_Roles.ModifiedBy = zReader["ModifiedBy"].ToString();
                    Group_Roles.ModifiedName = zReader["ModifiedName"].ToString();
                    _Message = "200 OK";
                }
                else
                {
                    _Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }

        #endregion

        #region [ Constructor Update Information ]

        public string Create_ServerKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO SYS_Group_Roles ("
         + " GroupKey , RoleKey , RoleRead , RoleAdd , RoleEdit , RoleDel , RoleApprove , RoleName , RoleID , RoleURL , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @GroupKey , @RoleKey , @RoleRead , @RoleAdd , @RoleEdit , @RoleDel , @RoleApprove , @RoleName , @RoleID , @RoleURL , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (Group_Roles.GroupKey != "" && Group_Roles.GroupKey.Length == 36)
                {
                    zCommand.Parameters.Add("@GroupKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Group_Roles.GroupKey);
                }
                else
                {
                    zCommand.Parameters.Add("@GroupKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                if (Group_Roles.RoleKey != "" && Group_Roles.RoleKey.Length == 36)
                {
                    zCommand.Parameters.Add("@RoleKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Group_Roles.RoleKey);
                }
                else
                {
                    zCommand.Parameters.Add("@RoleKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                if (Group_Roles.RoleRead == null)
                {
                    zCommand.Parameters.Add("@RoleRead", SqlDbType.Bit).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@RoleRead", SqlDbType.Bit).Value = Group_Roles.RoleRead;
                }

                if (Group_Roles.RoleAdd == null)
                {
                    zCommand.Parameters.Add("@RoleAdd", SqlDbType.Bit).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@RoleAdd", SqlDbType.Bit).Value = Group_Roles.RoleAdd;
                }

                if (Group_Roles.RoleEdit == null)
                {
                    zCommand.Parameters.Add("@RoleEdit", SqlDbType.Bit).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@RoleEdit", SqlDbType.Bit).Value = Group_Roles.RoleEdit;
                }

                if (Group_Roles.RoleDel == null)
                {
                    zCommand.Parameters.Add("@RoleDel", SqlDbType.Bit).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@RoleDel", SqlDbType.Bit).Value = Group_Roles.RoleDel;
                }

                if (Group_Roles.RoleApprove == null)
                {
                    zCommand.Parameters.Add("@RoleApprove", SqlDbType.Bit).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@RoleApprove", SqlDbType.Bit).Value = Group_Roles.RoleApprove;
                }

                zCommand.Parameters.Add("@RoleName", SqlDbType.NVarChar).Value = Group_Roles.RoleName;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = Group_Roles.RoleID;
                zCommand.Parameters.Add("@RoleURL", SqlDbType.NVarChar).Value = Group_Roles.RoleURL;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Group_Roles.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Group_Roles.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Group_Roles.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Group_Roles.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Group_Roles.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Create_ClientKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO SYS_Group_Roles("
         + " AutoKey , GroupKey , RoleKey , RoleRead , RoleAdd , RoleEdit , RoleDel , RoleApprove , RoleName , RoleID , RoleURL , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @AutoKey , @GroupKey , @RoleKey , @RoleRead , @RoleAdd , @RoleEdit , @RoleDel , @RoleApprove , @RoleName , @RoleID , @RoleURL , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Group_Roles.AutoKey;
                if (Group_Roles.GroupKey != "" && Group_Roles.GroupKey.Length == 36)
                {
                    zCommand.Parameters.Add("@GroupKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Group_Roles.GroupKey);
                }
                else
                {
                    zCommand.Parameters.Add("@GroupKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                if (Group_Roles.RoleKey != "" && Group_Roles.RoleKey.Length == 36)
                {
                    zCommand.Parameters.Add("@RoleKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Group_Roles.RoleKey);
                }
                else
                {
                    zCommand.Parameters.Add("@RoleKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                if (Group_Roles.RoleRead == null)
                {
                    zCommand.Parameters.Add("@RoleRead", SqlDbType.Bit).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@RoleRead", SqlDbType.Bit).Value = Group_Roles.RoleRead;
                }

                if (Group_Roles.RoleAdd == null)
                {
                    zCommand.Parameters.Add("@RoleAdd", SqlDbType.Bit).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@RoleAdd", SqlDbType.Bit).Value = Group_Roles.RoleAdd;
                }

                if (Group_Roles.RoleEdit == null)
                {
                    zCommand.Parameters.Add("@RoleEdit", SqlDbType.Bit).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@RoleEdit", SqlDbType.Bit).Value = Group_Roles.RoleEdit;
                }

                if (Group_Roles.RoleDel == null)
                {
                    zCommand.Parameters.Add("@RoleDel", SqlDbType.Bit).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@RoleDel", SqlDbType.Bit).Value = Group_Roles.RoleDel;
                }

                if (Group_Roles.RoleApprove == null)
                {
                    zCommand.Parameters.Add("@RoleApprove", SqlDbType.Bit).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@RoleApprove", SqlDbType.Bit).Value = Group_Roles.RoleApprove;
                }

                zCommand.Parameters.Add("@RoleName", SqlDbType.NVarChar).Value = Group_Roles.RoleName;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = Group_Roles.RoleID;
                zCommand.Parameters.Add("@RoleURL", SqlDbType.NVarChar).Value = Group_Roles.RoleURL;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Group_Roles.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Group_Roles.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Group_Roles.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Group_Roles.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Group_Roles.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Update()
        {
            string zSQL = "UPDATE SYS_Group_Roles SET "
                        + " GroupKey = @GroupKey,"
                        + " RoleKey = @RoleKey,"
                        + " RoleRead = @RoleRead,"
                        + " RoleAdd = @RoleAdd,"
                        + " RoleEdit = @RoleEdit,"
                        + " RoleDel = @RoleDel,"
                        + " RoleApprove = @RoleApprove,"
                        + " RoleName = @RoleName,"
                        + " RoleID = @RoleID,"
                        + " RoleURL = @RoleURL,"
                        + " RecordStatus = @RecordStatus,"
                        + " ModifiedOn = GetDate(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                        + " WHERE AutoKey = @AutoKey";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Group_Roles.AutoKey;
                if (Group_Roles.GroupKey != "" && Group_Roles.GroupKey.Length == 36)
                {
                    zCommand.Parameters.Add("@GroupKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Group_Roles.GroupKey);
                }
                else
                {
                    zCommand.Parameters.Add("@GroupKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                if (Group_Roles.RoleKey != "" && Group_Roles.RoleKey.Length == 36)
                {
                    zCommand.Parameters.Add("@RoleKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Group_Roles.RoleKey);
                }
                else
                {
                    zCommand.Parameters.Add("@RoleKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                if (Group_Roles.RoleRead == null)
                {
                    zCommand.Parameters.Add("@RoleRead", SqlDbType.Bit).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@RoleRead", SqlDbType.Bit).Value = Group_Roles.RoleRead;
                }

                if (Group_Roles.RoleAdd == null)
                {
                    zCommand.Parameters.Add("@RoleAdd", SqlDbType.Bit).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@RoleAdd", SqlDbType.Bit).Value = Group_Roles.RoleAdd;
                }

                if (Group_Roles.RoleEdit == null)
                {
                    zCommand.Parameters.Add("@RoleEdit", SqlDbType.Bit).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@RoleEdit", SqlDbType.Bit).Value = Group_Roles.RoleEdit;
                }

                if (Group_Roles.RoleDel == null)
                {
                    zCommand.Parameters.Add("@RoleDel", SqlDbType.Bit).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@RoleDel", SqlDbType.Bit).Value = Group_Roles.RoleDel;
                }

                if (Group_Roles.RoleApprove == null)
                {
                    zCommand.Parameters.Add("@RoleApprove", SqlDbType.Bit).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@RoleApprove", SqlDbType.Bit).Value = Group_Roles.RoleApprove;
                }

                zCommand.Parameters.Add("@RoleName", SqlDbType.NVarChar).Value = Group_Roles.RoleName;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = Group_Roles.RoleID;
                zCommand.Parameters.Add("@RoleURL", SqlDbType.NVarChar).Value = Group_Roles.RoleURL;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Group_Roles.RecordStatus;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Group_Roles.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Group_Roles.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE SYS_Group_Roles SET RecordStatus = 99 WHERE AutoKey = @AutoKey";
            string zConnectionString = ConfigurationManager.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Group_Roles.AutoKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Empty()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM SYS_Group_Roles WHERE AutoKey = @AutoKey";
            string zConnectionString = ConfigurationManager.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Group_Roles.AutoKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
