﻿namespace Admin
{
    public class User_Role
    {
        public string PartnerNumber { get; set; }
        public string UserKey { get; set; }
        public string RoleName { get; set; }
        public string RoleID { get; set; }
        public string RoleURL { get; set; }
        public string Module { get; set; }
        public string RoleKey { get; set; }
        public string Description { get; set; }
        public string Parent { get; set; }
        public int Level { get; set; }

        public string RouteName { get; set; }
        public string ActionName { get; set; }
        public string ControllerName { get; set; }
        public string ParamName { get; set; }
        public string Icon { get; set; }

        public bool RoleRead { get; set; } = false;
        public bool RoleAdd { get; set; } = false;
        public bool RoleEdit { get; set; } = false;
        public bool RoleDel { get; set; } = false;
        public bool RoleExe { get; set; } = false;
    }
}