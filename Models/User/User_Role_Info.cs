﻿using System;
using System.Data;
using System.Data.SqlClient;
namespace Admin
{
    public class User_Role_Info
    {

        public User_Role_Model User_Role = new User_Role_Model();
        private string _Message = "";
        public string Code
        {
            get
            {
                if (_Message.Length >= 3)
                {
                    return _Message.Substring(0, 3);
                }
                else
                {
                    return "";
                }
            }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        #region [ Constructor Get Information ]
        public User_Role_Info()
        {
            User_Role.UserKey = Guid.NewGuid().ToString();
        }
        public User_Role_Info(string UserKey)
        {
            string zSQL = "SELECT * FROM SYS_User_Role WHERE UserKey = @UserKey AND RecordStatus != 99 ";
            string zConnectionString = ConfigurationManager.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(UserKey);
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    User_Role.UserKey = zReader["UserKey"].ToString();
                    User_Role.RoleKey = zReader["RoleKey"].ToString();
                    if (zReader["RoleAll"] != DBNull.Value)
                    {
                        User_Role.RoleAll = (bool)zReader["RoleAll"];
                    }

                    if (zReader["RoleRead"] != DBNull.Value)
                    {
                        User_Role.RoleRead = (bool)zReader["RoleRead"];
                    }

                    if (zReader["RoleAdd"] != DBNull.Value)
                    {
                        User_Role.RoleAdd = (bool)zReader["RoleAdd"];
                    }

                    if (zReader["RoleEdit"] != DBNull.Value)
                    {
                        User_Role.RoleEdit = (bool)zReader["RoleEdit"];
                    }

                    if (zReader["RoleDel"] != DBNull.Value)
                    {
                        User_Role.RoleDel = (bool)zReader["RoleDel"];
                    }

                    if (zReader["RoleApprove"] != DBNull.Value)
                    {
                        User_Role.RoleApprove = (bool)zReader["RoleApprove"];
                    }

                    User_Role.PartnerNumber = zReader["PartnerNumber"].ToString();
                    User_Role.Module = zReader["Module"].ToString();
                    if (zReader["RecordStatus"] != DBNull.Value)
                    {
                        User_Role.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    }

                    User_Role.CreatedBy = zReader["CreatedBy"].ToString();
                    User_Role.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["CreatedOn"] != DBNull.Value)
                    {
                        User_Role.CreatedOn = (DateTime)zReader["CreatedOn"];
                    }

                    User_Role.ModifiedBy = zReader["ModifiedBy"].ToString();
                    User_Role.ModifiedName = zReader["ModifiedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                    {
                        User_Role.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    }

                    _Message = "200 OK";
                }
                else
                {
                    _Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }

        #endregion

        #region [ Constructor Update Information ]

        public string Create_ServerKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO SYS_User_Role ("
         + " RoleKey , RoleAll , RoleRead , RoleAdd , RoleEdit , RoleDel , RoleApprove , PartnerNumber , Module , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName"
         + " VALUES ( "
         + " @RoleKey , @RoleAll , @RoleRead , @RoleAdd , @RoleEdit , @RoleDel , @RoleApprove , @PartnerNumber , @Module , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName";
        string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (User_Role.RoleKey != "" && User_Role.RoleKey.Length == 36)
                {
                    zCommand.Parameters.Add("@RoleKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(User_Role.RoleKey);
                }
                else
                {
                    zCommand.Parameters.Add("@RoleKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                if (User_Role.RoleAll == null)
                {
                    zCommand.Parameters.Add("@RoleAll", SqlDbType.Bit).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@RoleAll", SqlDbType.Bit).Value = User_Role.RoleAll;
                }

                if (User_Role.RoleRead == null)
                {
                    zCommand.Parameters.Add("@RoleRead", SqlDbType.Bit).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@RoleRead", SqlDbType.Bit).Value = User_Role.RoleRead;
                }

                if (User_Role.RoleAdd == null)
                {
                    zCommand.Parameters.Add("@RoleAdd", SqlDbType.Bit).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@RoleAdd", SqlDbType.Bit).Value = User_Role.RoleAdd;
                }

                if (User_Role.RoleEdit == null)
                {
                    zCommand.Parameters.Add("@RoleEdit", SqlDbType.Bit).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@RoleEdit", SqlDbType.Bit).Value = User_Role.RoleEdit;
                }

                if (User_Role.RoleDel == null)
                {
                    zCommand.Parameters.Add("@RoleDel", SqlDbType.Bit).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@RoleDel", SqlDbType.Bit).Value = User_Role.RoleDel;
                }

                if (User_Role.RoleApprove == null)
                {
                    zCommand.Parameters.Add("@RoleApprove", SqlDbType.Bit).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@RoleApprove", SqlDbType.Bit).Value = User_Role.RoleApprove;
                }

                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = User_Role.PartnerNumber;
                zCommand.Parameters.Add("@Module", SqlDbType.NVarChar).Value = User_Role.Module;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = User_Role.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = User_Role.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = User_Role.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = User_Role.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = User_Role.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Create_ClientKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO SYS_User_Role("
         + " UserKey , RoleKey , RoleAll , RoleRead , RoleAdd , RoleEdit , RoleDel , RoleApprove , PartnerNumber , Module , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName"
         + " VALUES ( "
         + " @UserKey , @RoleKey , @RoleAll , @RoleRead , @RoleAdd , @RoleEdit , @RoleDel , @RoleApprove , @PartnerNumber , @Module , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName";
        string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (User_Role.UserKey != "" && User_Role.UserKey.Length == 36)
                {
                    zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(User_Role.UserKey);
                }
                else
                {
                    zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                if (User_Role.RoleKey != "" && User_Role.RoleKey.Length == 36)
                {
                    zCommand.Parameters.Add("@RoleKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(User_Role.RoleKey);
                }
                else
                {
                    zCommand.Parameters.Add("@RoleKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                if (User_Role.RoleAll == null)
                {
                    zCommand.Parameters.Add("@RoleAll", SqlDbType.Bit).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@RoleAll", SqlDbType.Bit).Value = User_Role.RoleAll;
                }

                if (User_Role.RoleRead == null)
                {
                    zCommand.Parameters.Add("@RoleRead", SqlDbType.Bit).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@RoleRead", SqlDbType.Bit).Value = User_Role.RoleRead;
                }

                if (User_Role.RoleAdd == null)
                {
                    zCommand.Parameters.Add("@RoleAdd", SqlDbType.Bit).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@RoleAdd", SqlDbType.Bit).Value = User_Role.RoleAdd;
                }

                if (User_Role.RoleEdit == null)
                {
                    zCommand.Parameters.Add("@RoleEdit", SqlDbType.Bit).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@RoleEdit", SqlDbType.Bit).Value = User_Role.RoleEdit;
                }

                if (User_Role.RoleDel == null)
                {
                    zCommand.Parameters.Add("@RoleDel", SqlDbType.Bit).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@RoleDel", SqlDbType.Bit).Value = User_Role.RoleDel;
                }

                if (User_Role.RoleApprove == null)
                {
                    zCommand.Parameters.Add("@RoleApprove", SqlDbType.Bit).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@RoleApprove", SqlDbType.Bit).Value = User_Role.RoleApprove;
                }

                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = User_Role.PartnerNumber;
                zCommand.Parameters.Add("@Module", SqlDbType.NVarChar).Value = User_Role.Module;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = User_Role.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = User_Role.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = User_Role.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = User_Role.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = User_Role.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE SYS_User_Role SET "
                        + " RoleKey = @RoleKey,"
                        + " RoleAll = @RoleAll,"
                        + " RoleRead = @RoleRead,"
                        + " RoleAdd = @RoleAdd,"
                        + " RoleEdit = @RoleEdit,"
                        + " RoleDel = @RoleDel,"
                        + " RoleApprove = @RoleApprove,"
                        + " PartnerNumber = @PartnerNumber,"
                        + " Module = @Module,"
                        + " RecordStatus = @RecordStatus,"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName,"
                        + " ModifiedOn = GetDate()"
                        + " WHERE UserKey = @UserKey";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (User_Role.UserKey != "" && User_Role.UserKey.Length == 36)
                {
                    zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(User_Role.UserKey);
                }
                else
                {
                    zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                if (User_Role.RoleKey != "" && User_Role.RoleKey.Length == 36)
                {
                    zCommand.Parameters.Add("@RoleKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(User_Role.RoleKey);
                }
                else
                {
                    zCommand.Parameters.Add("@RoleKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                if (User_Role.RoleAll == null)
                {
                    zCommand.Parameters.Add("@RoleAll", SqlDbType.Bit).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@RoleAll", SqlDbType.Bit).Value = User_Role.RoleAll;
                }

                if (User_Role.RoleRead == null)
                {
                    zCommand.Parameters.Add("@RoleRead", SqlDbType.Bit).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@RoleRead", SqlDbType.Bit).Value = User_Role.RoleRead;
                }

                if (User_Role.RoleAdd == null)
                {
                    zCommand.Parameters.Add("@RoleAdd", SqlDbType.Bit).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@RoleAdd", SqlDbType.Bit).Value = User_Role.RoleAdd;
                }

                if (User_Role.RoleEdit == null)
                {
                    zCommand.Parameters.Add("@RoleEdit", SqlDbType.Bit).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@RoleEdit", SqlDbType.Bit).Value = User_Role.RoleEdit;
                }

                if (User_Role.RoleDel == null)
                {
                    zCommand.Parameters.Add("@RoleDel", SqlDbType.Bit).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@RoleDel", SqlDbType.Bit).Value = User_Role.RoleDel;
                }

                if (User_Role.RoleApprove == null)
                {
                    zCommand.Parameters.Add("@RoleApprove", SqlDbType.Bit).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@RoleApprove", SqlDbType.Bit).Value = User_Role.RoleApprove;
                }

                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = User_Role.PartnerNumber;
                zCommand.Parameters.Add("@Module", SqlDbType.NVarChar).Value = User_Role.Module;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = User_Role.RecordStatus;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = User_Role.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = User_Role.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE SYS_User_Role SET RecordStatus = 99 WHERE UserKey = @UserKey";
            string zConnectionString = ConfigurationManager.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(User_Role.UserKey);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Empty()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM SYS_User_Role WHERE UserKey = @UserKey";
            string zConnectionString = ConfigurationManager.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(User_Role.UserKey);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
