﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
namespace Admin
{
    public class User_Data
    {
        public static List<User_Model> List(string PartnerNumber)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.*
FROM SYS_User A
WHERE A.RecordStatus != 99  
AND A.PartnerNumber = @PartnerNumber ";
            string zConnectionString = ConfigurationManager.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            List<User_Model> zList = new List<User_Model>();
            if (zTable.Rows.Count > 0)
            {

                foreach (DataRow r in zTable.Rows)
                {
                    zList.Add(new User_Model()
                    {
                        UserKey = r["UserKey"].ToString(),
                        UserName = r["UserName"].ToString(),
                        Activate = r["Activate"].ToBool(),
                        LastLoginDate = r["LastLoginDate"].ToDate(),
                        FailedPasswordCount = r["FailedPasswordCount"].ToInt(),
                        ExpireDate = r["ExpireDate"].ToDate(),
                        PartnerNumber = r["PartnerNumber"].ToString(),
                    });
                }
            }
            return zList;
        }


        public static int UserCount(string PartnerNumber)
        {
            int zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = @"SELECT COUNT(*) FROM SYS_User WHERE A.RecordStatus != 99 AND A.PartnerNumber = @PartnerNumber ";
            string zConnectionString = ConfigurationManager.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zResult = zCommand.ExecuteScalar().ToInt();

                zCommand.Dispose();
            }
            catch (Exception)
            {

            }
            finally
            {
                zConnect.Close(); ;
            }
            return zResult;
        }
        public static int CheckUserName(string UserName)
        {
            int zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = @"SELECT COUNT(*) FROM SYS_User WHERE UserName = @UserName ";
            string zConnectionString = ConfigurationManager.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@UserName", SqlDbType.NVarChar).Value = UserName;
                zResult = zCommand.ExecuteScalar().ToInt();

                zCommand.Dispose();
            }
            catch (Exception)
            {

            }
            finally
            {
                zConnect.Close(); ;
            }
            return zResult;
        }
    }
}
