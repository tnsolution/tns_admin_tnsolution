﻿using System;
namespace Admin
{
    public class User_Role_Model
    {
        #region [ Field Name ]
        private string _UserKey = "";
        private string _RoleKey = "";
        private bool _RoleAll;
        private bool _RoleRead;
        private bool _RoleAdd;
        private bool _RoleEdit;
        private bool _RoleDel;
        private bool _RoleApprove;
        private string _PartnerNumber = "";
        private string _Module = "";
        private int _RecordStatus = 0;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _CreatedOn = DateTime.MinValue;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private DateTime _ModifiedOn = DateTime.MinValue;
        #endregion

        #region [ Properties ]
        public string UserKey
        {
            get { return _UserKey; }
            set { _UserKey = value; }
        }
        public string RoleKey
        {
            get { return _RoleKey; }
            set { _RoleKey = value; }
        }
        public bool RoleAll
        {
            get { return _RoleAll; }
            set { _RoleAll = value; }
        }
        public bool RoleRead
        {
            get { return _RoleRead; }
            set { _RoleRead = value; }
        }
        public bool RoleAdd
        {
            get { return _RoleAdd; }
            set { _RoleAdd = value; }
        }
        public bool RoleEdit
        {
            get { return _RoleEdit; }
            set { _RoleEdit = value; }
        }
        public bool RoleDel
        {
            get { return _RoleDel; }
            set { _RoleDel = value; }
        }
        public bool RoleApprove
        {
            get { return _RoleApprove; }
            set { _RoleApprove = value; }
        }
        public string PartnerNumber
        {
            get { return _PartnerNumber; }
            set { _PartnerNumber = value; }
        }
        public string Module
        {
            get { return _Module; }
            set { _Module = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        #endregion
    }
}
