﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
namespace Admin
{
    public class User_Role_Data
    {
        public static List<User_Role_Model> List(string PartnerNumber, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM SYS_User_Role WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ";
            string zConnectionString = ConfigurationManager.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<User_Role_Model> zList = new List<User_Role_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new User_Role_Model()
                {
                    UserKey = r["UserKey"].ToString(),
                    RoleKey = r["RoleKey"].ToString(),
                    RoleAll = r["RoleAll"].ToBool(),
                    RoleRead = r["RoleRead"].ToBool(),
                    RoleAdd = r["RoleAdd"].ToBool(),
                    RoleEdit = r["RoleEdit"].ToBool(),
                    RoleDel = r["RoleDel"].ToBool(),
                    RoleApprove = r["RoleApprove"].ToBool(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    Module = r["Module"].ToString(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                });
            }
            return zList;
        }
    }
}
