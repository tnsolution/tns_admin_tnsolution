﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace Admin
{
    public class User_Info
    {
        public User_Model User = new User_Model();
        private string _Message = string.Empty;

        public string Code
        {
            get
            {
                if (Message.Length >= 3)
                {
                    return Message.Substring(0, 3);
                }
                else
                {
                    return "";
                }
            }
        }

        public string Message
        {
            get
            {
                return _Message;
            }

            set
            {
                _Message = value;
            }
        }
        #region [ Constructor Get Information ]
        public User_Info()
        {
            User.UserKey = Guid.NewGuid().ToString();
        }
        public User_Info(string UserKey)
        {
            string zSQL = "SELECT * FROM SYS_User WHERE UserKey = @UserKey AND RecordStatus != 99 ";
            string zConnectionString = ConfigurationManager.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(UserKey);
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    User.UserKey = zReader["UserKey"].ToString();
                    User.UserAPI = zReader["UserAPI"].ToString();
                    User.UserName = zReader["UserName"].ToString();
                    User.Password = zReader["Password"].ToString();
                    User.PIN = zReader["PIN"].ToString();
                    User.Description = zReader["Description"].ToString();
                    User.GroupName = zReader["GroupName"].ToString();
                    User.PartnerNumber = zReader["PartnerNumber"].ToString();
                    if (zReader["BusinessKey"] != DBNull.Value)
                    {
                        User.BusinessKey = int.Parse(zReader["BusinessKey"].ToString());
                    }

                    if (zReader["Activate"] != DBNull.Value)
                    {
                        User.Activate = (bool)zReader["Activate"];
                    }

                    if (zReader["ExpireDate"] != DBNull.Value)
                    {
                        User.ExpireDate = (DateTime)zReader["ExpireDate"];
                    }

                    if (zReader["LastLoginDate"] != DBNull.Value)
                    {
                        User.LastLoginDate = (DateTime)zReader["LastLoginDate"];
                    }

                    if (zReader["FailedPasswordCount"] != DBNull.Value)
                    {
                        User.FailedPasswordCount = int.Parse(zReader["FailedPasswordCount"].ToString());
                    }

                    User.EmployeeKey = zReader["EmployeeKey"].ToString();
                    User.EmployeeID = zReader["EmployeeID"].ToString();
                    User.EmployeeName = zReader["EmployeeName"].ToString();
                    if (zReader["Slug"] != DBNull.Value)
                    {
                        User.Slug = int.Parse(zReader["Slug"].ToString());
                    }

                    if (zReader["RecordStatus"] != DBNull.Value)
                    {
                        User.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    }

                    if (zReader["CreatedOn"] != DBNull.Value)
                    {
                        User.CreatedOn = (DateTime)zReader["CreatedOn"];
                    }

                    User.CreatedBy = zReader["CreatedBy"].ToString();
                    User.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                    {
                        User.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    }

                    User.ModifiedBy = zReader["ModifiedBy"].ToString();
                    User.ModifiedName = zReader["ModifiedName"].ToString();
                    _Message = "200 OK";
                }
                else
                {
                    _Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }
        public User_Info(string UserName, string Password)
        {
            string zSQL = @"
SELECT * 
FROM SYS_User 
WHERE UserName = @UserName 
AND Password = @Password 
AND RecordStatus != 99 ";
            string zConnectionString = ConfigurationManager.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@UserName", SqlDbType.NVarChar).Value = UserName;
                zCommand.Parameters.Add("@Password", SqlDbType.NVarChar).Value = TN_Utils.HashPass(Password);
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    User.UserKey = zReader["UserKey"].ToString();
                    User.UserAPI = zReader["UserAPI"].ToString();
                    User.UserName = zReader["UserName"].ToString();
                    User.Password = zReader["Password"].ToString();
                    User.PIN = zReader["PIN"].ToString();
                    User.Description = zReader["Description"].ToString();
                    User.GroupName = zReader["GroupName"].ToString();
                    User.PartnerNumber = zReader["PartnerNumber"].ToString().ToUpper();
                    if (zReader["BusinessKey"] != DBNull.Value)
                    {
                        User.BusinessKey = int.Parse(zReader["BusinessKey"].ToString());
                    }

                    if (zReader["Activate"] != DBNull.Value)
                    {
                        User.Activate = (bool)zReader["Activate"];
                    }

                    if (zReader["ExpireDate"] != DBNull.Value)
                    {
                        User.ExpireDate = (DateTime)zReader["ExpireDate"];
                    }

                    if (zReader["LastLoginDate"] != DBNull.Value)
                    {
                        User.LastLoginDate = (DateTime)zReader["LastLoginDate"];
                    }

                    if (zReader["FailedPasswordCount"] != DBNull.Value)
                    {
                        User.FailedPasswordCount = int.Parse(zReader["FailedPasswordCount"].ToString());
                    }

                    User.EmployeeKey = zReader["EmployeeKey"].ToString();
                    User.EmployeeID = zReader["EmployeeID"].ToString();
                    User.EmployeeName = zReader["EmployeeName"].ToString();
                    if (zReader["Slug"] != DBNull.Value)
                    {
                        User.Slug = int.Parse(zReader["Slug"].ToString());
                    }

                    if (zReader["RecordStatus"] != DBNull.Value)
                    {
                        User.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    }

                    if (zReader["CreatedOn"] != DBNull.Value)
                    {
                        User.CreatedOn = (DateTime)zReader["CreatedOn"];
                    }

                    User.CreatedBy = zReader["CreatedBy"].ToString();
                    User.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                    {
                        User.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    }

                    User.ModifiedBy = zReader["ModifiedBy"].ToString();
                    User.ModifiedName = zReader["ModifiedName"].ToString();
                    _Message = "200 OK";
                }
                else
                {
                    _Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }
        public User_Info(string UserName, string Password, string PartnerNumber)
        {
            string zSQL = @"
SELECT A.*, B.PhotoPath, C.ItemKey, C.JsonKey
FROM SYS_User A 
LEFT JOIN HRM_Employee B ON A.EmployeeKey = B.EmployeeKey 
LEFT JOIN SYS_User_Access C ON A.UserKey = C.UserKey
WHERE A.UserName = @UserName 
AND A.Password = @Password 
AND A.PartnerNumber = @PartnerNumber 
AND A.RecordStatus != 99 ";

            string zConnectionString = ConfigurationManager.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@UserName", SqlDbType.NVarChar).Value = UserName;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@Password", SqlDbType.NVarChar).Value = TN_Utils.HashPass(Password);
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    User.DataAccess = zReader["JsonKey"].ToString();
                    User.UserKey = zReader["UserKey"].ToString();
                    User.UserAPI = zReader["UserAPI"].ToString();
                    User.UserName = zReader["UserName"].ToString();
                    User.Password = zReader["Password"].ToString();
                    User.PIN = zReader["PIN"].ToString();
                    User.Description = zReader["Description"].ToString();
                    User.GroupName = zReader["GroupName"].ToString();
                    User.PartnerNumber = zReader["PartnerNumber"].ToString().ToUpper();
                    if (zReader["BusinessKey"] != DBNull.Value)
                    {
                        User.BusinessKey = int.Parse(zReader["BusinessKey"].ToString());
                    }

                    if (zReader["Activate"] != DBNull.Value)
                    {
                        User.Activate = (bool)zReader["Activate"];
                    }

                    if (zReader["ExpireDate"] != DBNull.Value)
                    {
                        User.ExpireDate = (DateTime)zReader["ExpireDate"];
                    }

                    if (zReader["LastLoginDate"] != DBNull.Value)
                    {
                        User.LastLoginDate = (DateTime)zReader["LastLoginDate"];
                    }

                    if (zReader["FailedPasswordCount"] != DBNull.Value)
                    {
                        User.FailedPasswordCount = int.Parse(zReader["FailedPasswordCount"].ToString());
                    }

                    User.EmployeeKey = zReader["EmployeeKey"].ToString();
                    User.EmployeeID = zReader["EmployeeID"].ToString();
                    User.EmployeeName = zReader["EmployeeName"].ToString();
                    User.PhotoPath = zReader["PhotoPath"].ToString();
                    if (zReader["Slug"] != DBNull.Value)
                    {
                        User.Slug = int.Parse(zReader["Slug"].ToString());
                    }

                    if (zReader["RecordStatus"] != DBNull.Value)
                    {
                        User.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    }

                    if (zReader["CreatedOn"] != DBNull.Value)
                    {
                        User.CreatedOn = (DateTime)zReader["CreatedOn"];
                    }

                    User.CreatedBy = zReader["CreatedBy"].ToString();
                    User.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                    {
                        User.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    }

                    User.ModifiedBy = zReader["ModifiedBy"].ToString();
                    User.ModifiedName = zReader["ModifiedName"].ToString();
                    _Message = "200 OK";
                }
                else
                {
                    _Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }
        #endregion

        #region [ Constructor Update Information ]

        public string Create_ServerKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO SYS_User ("
         + " UserAPI , UserName , Password , PIN , Description , GroupName , PartnerNumber , BusinessKey , Activate , ExpireDate , LastLoginDate , FailedPasswordCount , EmployeeKey , EmployeeID , EmployeeName , Slug , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @UserAPI , @UserName , @Password , @PIN , @Description , @GroupName , @PartnerNumber , @BusinessKey , @Activate , @ExpireDate , @LastLoginDate , @FailedPasswordCount , @EmployeeKey , @EmployeeID , @EmployeeName , @Slug , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@UserAPI", SqlDbType.NVarChar).Value = User.UserAPI;
                zCommand.Parameters.Add("@UserName", SqlDbType.NVarChar).Value = User.UserName;
                zCommand.Parameters.Add("@Password", SqlDbType.NVarChar).Value = User.Password;
                zCommand.Parameters.Add("@PIN", SqlDbType.NVarChar).Value = User.PIN;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = User.Description;
                zCommand.Parameters.Add("@GroupName", SqlDbType.NVarChar).Value = User.GroupName;
                if (User.PartnerNumber != "" && User.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(User.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@BusinessKey", SqlDbType.Int).Value = User.BusinessKey;
                zCommand.Parameters.Add("@Activate", SqlDbType.Bit).Value = User.Activate;
                if (User.ExpireDate == null)
                {
                    zCommand.Parameters.Add("@ExpireDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@ExpireDate", SqlDbType.DateTime).Value = User.ExpireDate;
                }

                if (User.LastLoginDate == null)
                {
                    zCommand.Parameters.Add("@LastLoginDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@LastLoginDate", SqlDbType.DateTime).Value = User.LastLoginDate;
                }

                zCommand.Parameters.Add("@FailedPasswordCount", SqlDbType.Int).Value = User.FailedPasswordCount;
                if (User.EmployeeKey != "" && User.EmployeeKey.Length == 36)
                {
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(User.EmployeeKey);
                }
                else
                {
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = User.EmployeeID;
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = User.EmployeeName;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = User.Slug;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = User.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = User.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = User.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = User.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = User.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Create_ClientKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO SYS_User("
         + " UserKey , UserAPI , UserName , Password , PIN , Description , GroupName , PartnerNumber , BusinessKey , Activate , ExpireDate , LastLoginDate , FailedPasswordCount , EmployeeKey , EmployeeID , EmployeeName , Slug , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @UserKey , @UserAPI , @UserName , @Password , @PIN , @Description , @GroupName , @PartnerNumber , @BusinessKey , @Activate , @ExpireDate , @LastLoginDate , @FailedPasswordCount , @EmployeeKey , @EmployeeID , @EmployeeName , @Slug , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (User.UserKey != "" && User.UserKey.Length == 36)
                {
                    zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(User.UserKey);
                }
                else
                {
                    zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@UserAPI", SqlDbType.NVarChar).Value = User.UserAPI;
                zCommand.Parameters.Add("@UserName", SqlDbType.NVarChar).Value = User.UserName;
                zCommand.Parameters.Add("@Password", SqlDbType.NVarChar).Value = User.Password;
                zCommand.Parameters.Add("@PIN", SqlDbType.NVarChar).Value = User.PIN;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = User.Description;
                zCommand.Parameters.Add("@GroupName", SqlDbType.NVarChar).Value = User.GroupName;
                if (User.PartnerNumber != "" && User.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(User.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@BusinessKey", SqlDbType.Int).Value = User.BusinessKey;
                zCommand.Parameters.Add("@Activate", SqlDbType.Bit).Value = User.Activate;
                if (User.ExpireDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@ExpireDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@ExpireDate", SqlDbType.DateTime).Value = User.ExpireDate;
                }

                if (User.LastLoginDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@LastLoginDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@LastLoginDate", SqlDbType.DateTime).Value = User.LastLoginDate;
                }

                zCommand.Parameters.Add("@FailedPasswordCount", SqlDbType.Int).Value = User.FailedPasswordCount;
                if (User.EmployeeKey != "" && User.EmployeeKey.Length == 36)
                {
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(User.EmployeeKey);
                }
                else
                {
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = User.EmployeeID;
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = User.EmployeeName;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = User.Slug;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = User.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = User.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = User.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = User.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = User.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE SYS_User SET "
                        + " UserName = @UserName,"
                        + " EmployeeKey = @EmployeeKey, EmployeeName = @EmployeeName,"
                        + " ModifiedOn = GetDate(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                        + " WHERE UserKey = @UserKey";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (User.UserKey != "" && User.UserKey.Length == 36)
                {
                    zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(User.UserKey);
                }
                else
                {
                    zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@UserName", SqlDbType.NVarChar).Value = User.UserName;
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = User.EmployeeName;

                if (User.EmployeeKey != "" && User.EmployeeKey.Length == 36)
                {
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(User.EmployeeKey);
                }
                else
                {
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = User.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = User.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        public string ResetPass(string UserKey, string PasswordNew)
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = @"UPDATE SYS_User SET Password = @Password, ModifiedBy = @ModifiedBy, ModifiedName = @ModifiedName, ModifiedOn = GetDate() WHERE UserKey = @UserKey ";
            string zConnectionString = ConfigurationManager.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();

            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);

                zCommand.CommandType = CommandType.Text;

                zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = new Guid(UserKey);
                zCommand.Parameters.Add("@Password", SqlDbType.NVarChar).Value = PasswordNew;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = User.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = User.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                _Message = "200 OK";
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close(); ;
            }

            return zResult;

        }
        public string SetActivate(string UserKey)
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = @"UPDATE SYS_User SET Activate = (CASE Activate WHEN 1 THEN 0 ELSE 1 END),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName,"
                        + " ModifiedOn = GetDate()"
                        + " WHERE UserKey = @UserKey";
            string zConnectionString = ConfigurationManager.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(UserKey);
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = User.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = User.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                _Message = "200 OK";
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close(); ;
            }
            return zResult;
        }

        public string Delete(string UserKey)
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE SYS_User SET RecordStatus = 99 WHERE UserKey = @UserKey";
            string zConnectionString = ConfigurationManager.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(UserKey);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Empty()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM SYS_User WHERE UserKey = @UserKey";
            string zConnectionString = ConfigurationManager.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(User.UserKey);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        public string Delete_Role(string UserKey)
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM SYS_User_Role WHERE UserKey = @UserKey";
            string zConnectionString = ConfigurationManager.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@UserKey", SqlDbType.NVarChar).Value = UserKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete_Role(string UserKey, string RoleKey)
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM SYS_User_Role WHERE UserKey = @UserKey AND RoleKey = @RoleKey";
            string zConnectionString = ConfigurationManager.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@UserKey", SqlDbType.NVarChar).Value = UserKey;
                zCommand.Parameters.Add("@RoleKey", SqlDbType.NVarChar).Value = RoleKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Add_Role(string UserKey, string RoleKey, int Slug)
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO SYS_User_Role (UserKey, RoleKey, Slug) VALUES (@UserKey, @RoleKey, @Slug)";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@UserKey", SqlDbType.NVarChar).Value = UserKey;
                zCommand.Parameters.Add("@RoleKey", SqlDbType.NVarChar).Value = RoleKey;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = Slug;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public bool Exists_Role(string UserKey, string RoleKey)
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT COUNT(*) FROM SYS_User_Role WHERE UserKey = @UserKey AND RoleKey = @RoleKey";
            int zResult = 0;
            string zConnectionString = ConfigurationManager.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@UserKey", SqlDbType.NVarChar).Value = UserKey;
                zCommand.Parameters.Add("@RoleKey", SqlDbType.NVarChar).Value = DBNull.Value;
                zResult = zCommand.ExecuteScalar().ToInt();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            if (zResult > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public string Edit_Role(string UserKey, string RoleKey, bool Read, bool Add, bool Edit, bool Delete, bool Exe, int Slug)
        {
            //---------- String SQL Access Database ---------------
            string zSQL = @"
UPDATE SYS_User_Role SET 
RoleRead = @RoleRead,
RoleAdd = @RoleAdd,
RoleEdit = @RoleEdit,
RoleDel = @RoleDel,
RoleApprove = @RoleApprove,
Slug = @Slug
WHERE UserKey = @UserKey AND RoleKey = @RoleKey";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@UserKey", SqlDbType.NVarChar).Value = UserKey;
                zCommand.Parameters.Add("@RoleKey", SqlDbType.NVarChar).Value = RoleKey;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = Slug;

                zCommand.Parameters.Add("@RoleRead", SqlDbType.Bit).Value = Read;
                zCommand.Parameters.Add("@RoleAdd", SqlDbType.Bit).Value = Add;
                zCommand.Parameters.Add("@RoleEdit", SqlDbType.Bit).Value = Edit;
                zCommand.Parameters.Add("@RoleDel", SqlDbType.Bit).Value = Delete;
                zCommand.Parameters.Add("@RoleApprove", SqlDbType.Bit).Value = Exe;

                zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
