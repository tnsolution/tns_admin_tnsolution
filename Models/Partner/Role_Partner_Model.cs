﻿namespace Admin
{
    public class Role_Partner_Model
    {
        #region [ Field Name ]
        private string _RoleKey = "";
        private string _Parent = "";
        private string _RoleName = "";
        private string _PartnerNumber = "";
        private string _Module = "";
        private int _Level = 0;
        #endregion

        #region [ Properties ]
        public string RoleKey
        {
            get { return _RoleKey; }
            set { _RoleKey = value; }
        }
        public string PartnerNumber
        {
            get { return _PartnerNumber; }
            set { _PartnerNumber = value; }
        }
        public string Module
        {
            get { return _Module; }
            set { _Module = value; }
        }

        public string Parent
        {
            get
            {
                return _Parent;
            }

            set
            {
                _Parent = value;
            }
        }

        public string RoleName
        {
            get
            {
                return _RoleName;
            }

            set
            {
                _RoleName = value;
            }
        }

        public int Level
        {
            get
            {
                return _Level;
            }

            set
            {
                _Level = value;
            }
        }
        #endregion
    }
}
