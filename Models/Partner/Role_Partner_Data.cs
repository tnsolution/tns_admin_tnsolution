﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
namespace Admin
{
    public class Role_Partner_Data
    {
        public static List<Role_Partner_Model> List(string PartnerNumber, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM SYS_Role_Partner WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ";
            string zConnectionString = ConfigurationManager.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Role_Partner_Model> zList = new List<Role_Partner_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Role_Partner_Model()
                {
                    RoleKey = r["RoleKey"].ToString(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    Module = r["Module"].ToString(),
                });
            }
            return zList;
        }
    }
}
