﻿using System;
using System.Data;
using System.Data.SqlClient;
namespace Admin
{
    public class Partner_Info
    {
        public Partner_Model Partner = new Partner_Model();
        private string _Message = "";
        public string Code
        {
            get
            {
                if (_Message.Length >= 3)
                {
                    return _Message.Substring(0, 3);
                }
                else
                {
                    return "";
                }
            }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        #region [ Constructor Get Information ]
        public Partner_Info()
        {
            Partner.PartnerNumber = Guid.NewGuid().ToString();
        }
        public Partner_Info(string PartnerNumber)
        {
            string zSQL = "SELECT * FROM SYS_Partner WHERE PartnerNumber = @PartnerNumber AND RecordStatus != 99 ";
            string zConnectionString = ConfigurationManager.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(PartnerNumber);
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    Partner.BillConfig = zReader["BillConfig"].ToString();
                    Partner.ModuleRole = zReader["ModuleRole"].ToString();
                    Partner.ModuleRoleMobile = zReader["ModuleRoleMobile"].ToString();

                    Partner.PartnerNumber = zReader["PartnerNumber"].ToString();
                    Partner.PartnerName = zReader["PartnerName"].ToString();
                    Partner.PartnerAddress = zReader["PartnerAddress"].ToString();
                    Partner.PartnerPhone = zReader["PartnerPhone"].ToString();
                    Partner.PartnerID = zReader["PartnerID"].ToString();
                    Partner.StoreID = zReader["StoreID"].ToString();
                    Partner.StoreName = zReader["StoreName"].ToString();
                    Partner.Parent = zReader["Parent"].ToString();
                    if (zReader["Activated"] != DBNull.Value)
                    {
                        Partner.Activated = (bool)zReader["Activated"];
                    }

                    if (zReader["ActivationDate"] != DBNull.Value)
                    {
                        Partner.ActivationDate = (DateTime)zReader["ActivationDate"];
                    }

                    Partner.Description = zReader["Description"].ToString();
                    if (zReader["BusinessKey"] != DBNull.Value)
                    {
                        Partner.BusinessKey = int.Parse(zReader["BusinessKey"].ToString());
                    }

                    Partner.BusinessName = zReader["BusinessName"].ToString();
                    Partner.Logo_Small = zReader["Logo_Small"].ToString();
                    Partner.Logo_Large = zReader["Logo_Large"].ToString();
                    Partner.Logo_Mobile = zReader["Logo_Mobile"].ToString();
                    if (zReader["AccountAmount"] != DBNull.Value)
                    {
                        Partner.AccountAmount = int.Parse(zReader["AccountAmount"].ToString());
                    }

                    Partner.HomepageDesktop = zReader["HomepageDesktop"].ToString();
                    Partner.HomepageMobile = zReader["HomepageMobile"].ToString();
                    if (zReader["RecordStatus"] != DBNull.Value)
                    {
                        Partner.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    }

                    if (zReader["CreatedOn"] != DBNull.Value)
                    {
                        Partner.CreatedOn = (DateTime)zReader["CreatedOn"];
                    }

                    Partner.CreatedBy = zReader["CreatedBy"].ToString();
                    Partner.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                    {
                        Partner.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    }

                    Partner.ModifiedBy = zReader["ModifiedBy"].ToString();
                    Partner.ModifiedName = zReader["ModifiedName"].ToString();
                    _Message = "200 OK";
                }
                else
                {
                    _Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }
        #endregion

        #region [ Constructor Update Information ]
        public string Create_ServerKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO SYS_Partner ("
         + " PartnerName, ModuleRole, ModuleRoleMobile , PartnerAddress , PartnerPhone , PartnerID , StoreID , StoreName , Parent , Activated , ActivationDate , Description , BusinessKey , BusinessName , Logo_Small , Logo_Large , Logo_Mobile , AccountAmount , HomepageDesktop , HomepageMobile , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @PartnerName, @ModuleRole, @ModuleRoleMobile , @PartnerAddress , @PartnerPhone , @PartnerID , @StoreID , @StoreName , @Parent , @Activated , @ActivationDate , @Description , @BusinessKey , @BusinessName , @Logo_Small , @Logo_Large , @Logo_Mobile , @AccountAmount , @HomepageDesktop , @HomepageMobile , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ModuleRoleMobile", SqlDbType.NVarChar).Value = Partner.ModuleRoleMobile;
                zCommand.Parameters.Add("@ModuleRole", SqlDbType.NVarChar).Value = Partner.ModuleRole;
                zCommand.Parameters.Add("@PartnerName", SqlDbType.NVarChar).Value = Partner.PartnerName;
                zCommand.Parameters.Add("@PartnerAddress", SqlDbType.NVarChar).Value = Partner.PartnerAddress;
                zCommand.Parameters.Add("@PartnerPhone", SqlDbType.NVarChar).Value = Partner.PartnerPhone;
                zCommand.Parameters.Add("@PartnerID", SqlDbType.NVarChar).Value = Partner.PartnerID;
                zCommand.Parameters.Add("@StoreID", SqlDbType.NVarChar).Value = Partner.StoreID;
                zCommand.Parameters.Add("@StoreName", SqlDbType.NVarChar).Value = Partner.StoreName;
                zCommand.Parameters.Add("@Parent", SqlDbType.NVarChar).Value = Partner.Parent;
                zCommand.Parameters.Add("@Activated", SqlDbType.Bit).Value = Partner.Activated;
                if (Partner.ActivationDate == null)
                {
                    zCommand.Parameters.Add("@ActivationDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@ActivationDate", SqlDbType.DateTime).Value = Partner.ActivationDate;
                }

                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Partner.Description;
                zCommand.Parameters.Add("@BusinessKey", SqlDbType.Int).Value = Partner.BusinessKey;
                zCommand.Parameters.Add("@BusinessName", SqlDbType.NVarChar).Value = Partner.BusinessName;
                zCommand.Parameters.Add("@Logo_Small", SqlDbType.NVarChar).Value = Partner.Logo_Small;
                zCommand.Parameters.Add("@Logo_Large", SqlDbType.NVarChar).Value = Partner.Logo_Large;
                zCommand.Parameters.Add("@Logo_Mobile", SqlDbType.NVarChar).Value = Partner.Logo_Mobile;
                zCommand.Parameters.Add("@AccountAmount", SqlDbType.Int).Value = Partner.AccountAmount;
                zCommand.Parameters.Add("@HomepageDesktop", SqlDbType.NVarChar).Value = Partner.HomepageDesktop;
                zCommand.Parameters.Add("@HomepageMobile", SqlDbType.NVarChar).Value = Partner.HomepageMobile;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Partner.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Partner.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Partner.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Partner.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Partner.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Create_ClientKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO SYS_Partner("
         + " PartnerNumber, ModuleRole, ModuleRoleMobile, PartnerName , PartnerAddress , PartnerPhone , PartnerID , StoreID , StoreName , Parent , Activated , ActivationDate , Description , BusinessKey , BusinessName , Logo_Small , Logo_Large , Logo_Mobile , AccountAmount , HomepageDesktop , HomepageMobile , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @PartnerNumber, @ModuleRole, @ModuleRoleMobile, @PartnerName , @PartnerAddress , @PartnerPhone , @PartnerID , @StoreID , @StoreName , @Parent , @Activated , @ActivationDate , @Description , @BusinessKey , @BusinessName , @Logo_Small , @Logo_Large , @Logo_Mobile , @AccountAmount , @HomepageDesktop , @HomepageMobile , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ModuleRoleMobile", SqlDbType.NVarChar).Value = Partner.ModuleRoleMobile;
                zCommand.Parameters.Add("@ModuleRole", SqlDbType.NVarChar).Value = Partner.ModuleRole;
                if (Partner.PartnerNumber != "" && Partner.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Partner.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@PartnerName", SqlDbType.NVarChar).Value = Partner.PartnerName;
                zCommand.Parameters.Add("@PartnerAddress", SqlDbType.NVarChar).Value = Partner.PartnerAddress;
                zCommand.Parameters.Add("@PartnerPhone", SqlDbType.NVarChar).Value = Partner.PartnerPhone;
                zCommand.Parameters.Add("@PartnerID", SqlDbType.NVarChar).Value = Partner.PartnerID;
                zCommand.Parameters.Add("@StoreID", SqlDbType.NVarChar).Value = Partner.StoreID;
                zCommand.Parameters.Add("@StoreName", SqlDbType.NVarChar).Value = Partner.StoreName;
                zCommand.Parameters.Add("@Parent", SqlDbType.NVarChar).Value = Partner.Parent;
                zCommand.Parameters.Add("@Activated", SqlDbType.Bit).Value = Partner.Activated;
                if (Partner.ActivationDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@ActivationDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@ActivationDate", SqlDbType.DateTime).Value = Partner.ActivationDate;
                }

                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Partner.Description;
                zCommand.Parameters.Add("@BusinessKey", SqlDbType.Int).Value = Partner.BusinessKey;
                zCommand.Parameters.Add("@BusinessName", SqlDbType.NVarChar).Value = Partner.BusinessName;
                zCommand.Parameters.Add("@Logo_Small", SqlDbType.NVarChar).Value = Partner.Logo_Small;
                zCommand.Parameters.Add("@Logo_Large", SqlDbType.NVarChar).Value = Partner.Logo_Large;
                zCommand.Parameters.Add("@Logo_Mobile", SqlDbType.NVarChar).Value = Partner.Logo_Mobile;
                zCommand.Parameters.Add("@AccountAmount", SqlDbType.Int).Value = Partner.AccountAmount;
                zCommand.Parameters.Add("@HomepageDesktop", SqlDbType.NVarChar).Value = Partner.HomepageDesktop;
                zCommand.Parameters.Add("@HomepageMobile", SqlDbType.NVarChar).Value = Partner.HomepageMobile;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Partner.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Partner.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Partner.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Partner.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Partner.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE SYS_Partner SET "
                        + " PartnerName = @PartnerName,"
                        + " PartnerAddress = @PartnerAddress,"
                        + " PartnerPhone = @PartnerPhone,"
                        + " PartnerID = @PartnerID,"
                        + " StoreID = @StoreID,"
                        + " StoreName = @StoreName,"
                        + " Parent = @Parent,"
                        + " Activated = @Activated,"
                        + " ActivationDate = @ActivationDate,"
                        + " Description = @Description,"
                        + " BusinessKey = @BusinessKey,"
                        + " BusinessName = @BusinessName,"
                        + " Logo_Small = @Logo_Small,"
                        + " Logo_Large = @Logo_Large,"
                        + " Logo_Mobile = @Logo_Mobile,"
                        + " AccountAmount = @AccountAmount,"
                        + " HomepageDesktop = @HomepageDesktop,"
                        + " HomepageMobile = @HomepageMobile,"
                        + " RecordStatus = @RecordStatus,"
                        + " ModifiedOn = GetDate(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                        + " WHERE PartnerNumber = @PartnerNumber";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ModuleRoleMobile", SqlDbType.NVarChar).Value = Partner.ModuleRoleMobile;
                zCommand.Parameters.Add("@ModuleRole", SqlDbType.NVarChar).Value = Partner.ModuleRole;
                if (Partner.PartnerNumber != "" && Partner.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Partner.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@PartnerName", SqlDbType.NVarChar).Value = Partner.PartnerName;
                zCommand.Parameters.Add("@PartnerAddress", SqlDbType.NVarChar).Value = Partner.PartnerAddress;
                zCommand.Parameters.Add("@PartnerPhone", SqlDbType.NVarChar).Value = Partner.PartnerPhone;
                zCommand.Parameters.Add("@PartnerID", SqlDbType.NVarChar).Value = Partner.PartnerID;
                zCommand.Parameters.Add("@StoreID", SqlDbType.NVarChar).Value = Partner.StoreID;
                zCommand.Parameters.Add("@StoreName", SqlDbType.NVarChar).Value = Partner.StoreName;
                zCommand.Parameters.Add("@Parent", SqlDbType.NVarChar).Value = Partner.Parent;
                zCommand.Parameters.Add("@Activated", SqlDbType.Bit).Value = Partner.Activated;
                if (Partner.ActivationDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@ActivationDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@ActivationDate", SqlDbType.DateTime).Value = Partner.ActivationDate;
                }

                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Partner.Description;
                zCommand.Parameters.Add("@BusinessKey", SqlDbType.Int).Value = Partner.BusinessKey;
                zCommand.Parameters.Add("@BusinessName", SqlDbType.NVarChar).Value = Partner.BusinessName;
                zCommand.Parameters.Add("@Logo_Small", SqlDbType.NVarChar).Value = Partner.Logo_Small;
                zCommand.Parameters.Add("@Logo_Large", SqlDbType.NVarChar).Value = Partner.Logo_Large;
                zCommand.Parameters.Add("@Logo_Mobile", SqlDbType.NVarChar).Value = Partner.Logo_Mobile;
                zCommand.Parameters.Add("@AccountAmount", SqlDbType.Int).Value = Partner.AccountAmount;
                zCommand.Parameters.Add("@HomepageDesktop", SqlDbType.NVarChar).Value = Partner.HomepageDesktop;
                zCommand.Parameters.Add("@HomepageMobile", SqlDbType.NVarChar).Value = Partner.HomepageMobile;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Partner.RecordStatus;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Partner.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Partner.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE SYS_Partner SET RecordStatus = 99 WHERE PartnerNumber = @PartnerNumber";
            string zConnectionString = ConfigurationManager.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Partner.PartnerNumber);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Empty()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM SYS_Partner WHERE PartnerNumber = @PartnerNumber";
            string zConnectionString = ConfigurationManager.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Partner.PartnerNumber);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string AutoID()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.Auto_PartnerID()";
            string zConnectionString = ConfigurationManager.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zResult = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string UpdateManual()
        {
            string zSQL = "UPDATE SYS_Partner SET "
                        + " PartnerAddress = @PartnerAddress,"
                        + " PartnerPhone = @PartnerPhone,"
                        + " StoreName = @StoreName, PartnerName = @PartnerName,"
                        + " Logo_Large = @Logo_Large,"
                        + " ModifiedOn = GetDate(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                        + " WHERE PartnerNumber = @PartnerNumber";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Partner.PartnerNumber);
                zCommand.Parameters.Add("@PartnerName", SqlDbType.NVarChar).Value = Partner.PartnerName;
                zCommand.Parameters.Add("@PartnerAddress", SqlDbType.NVarChar).Value = Partner.PartnerAddress;
                zCommand.Parameters.Add("@PartnerPhone", SqlDbType.NVarChar).Value = Partner.PartnerPhone;
                zCommand.Parameters.Add("@StoreName", SqlDbType.NVarChar).Value = Partner.StoreName;
                zCommand.Parameters.Add("@Logo_Large", SqlDbType.NVarChar).Value = Partner.Logo_Large;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Partner.RecordStatus;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Partner.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Partner.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string UpdateBill(string Data)
        {
            string zSQL = "UPDATE SYS_Partner SET "
                        + " BillConfig = @BillConfig,"
                        + " ModifiedOn = GetDate(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                        + " WHERE PartnerNumber = @PartnerNumber";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Partner.PartnerNumber);
                zCommand.Parameters.Add("@BillConfig", SqlDbType.NVarChar).Value = Data;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Partner.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Partner.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
