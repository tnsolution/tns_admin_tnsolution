﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Admin
{
    public class Authen
    {
        public static List<string> GetModule(out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT DISTINCT Module FROM SYS_Role WHERE RecordStatus != 99 ORDER BY Module";
            string zConnectionString = ConfigurationManager.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<string> zList = new List<string>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(r["Module"].ToString());
            }
            return zList;
        }
        public static List<Role_Model> Recursive_Role(string Module)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"; 
WITH R AS (
	-- ANCHOR PART
      SELECT 
		  RoleKey, 
		  RoleName AS NAME, RouteName, ActionName, ControllerName, Icon, RoleID, RoleURL, Description,
		  PARENT,
		  DEPTH = 0 , 
		  SORT = CAST(RoleKey AS VARCHAR(MAX))
      FROM SYS_Role
      WHERE 
	  RecordStatus <> 99
      AND Parent = '0'
	  AND module = @module
      UNION ALL
	-- RECURSIVE PART
      SELECT 		
		Sub.RoleKey, 
		Sub.RoleName AS NAME, Sub.RouteName, Sub.ActionName, Sub.ControllerName, Sub.Icon,  Sub.RoleID, Sub.RoleURL, Sub.Description,
		Sub.PARENT,
		DEPTH = R.DEPTH + 1, 
		SORT = R.SORT + '-' + CAST(Sub.RoleKey AS VARCHAR(MAX))
      FROM R
      INNER JOIN SYS_Role Sub ON CAST(R.RoleKey AS VARCHAR(MAX)) = Sub.Parent
	  WHERE 
	  RecordStatus <> 99
	  AND module = @module
) 
SELECT TREE = REPLICATE('---',R.DEPTH*1)+R.[NAME], R.RoleKey, R.RouteName, R.ActionName, R.ControllerName, R.Icon, R.RoleID, R.RoleURL, R.Description
FROM R
ORDER BY SORT";
            string zConnectionString = ConfigurationManager.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Module", SqlDbType.NVarChar).Value = Module;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            List<Role_Model> zList = new List<Role_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Role_Model()
                {
                    RoleName = r["TREE"].ToString(),
                    RoleKey = r["RoleKey"].ToString(),
                    RoleID = r["RoleID"].ToString(),
                    RoleURL = r["RoleURL"].ToString(),
                    ControllerName = r["ControllerName"].ToString(),
                    ActionName = r["ActionName"].ToString(),
                    RouteName = r["RouteName"].ToString(),
                    Icon = r["Icon"].ToString(),
                    Description = r["Description"].ToString(),
                });
            }

            return zList;
        }
        public static List<Role_Model> Recursive_Role(string Module, int Slug)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"; 
WITH R AS (
	-- ANCHOR PART
      SELECT 
		  RoleKey, 
		  RoleName AS NAME, RouteName, ActionName, ControllerName, Icon, RoleID, RoleURL, Description, Slug,
		  PARENT,
		  DEPTH = 0 , 
		  SORT = CAST(RoleKey AS VARCHAR(MAX))
      FROM SYS_Role
      WHERE 
	  RecordStatus <> 99
      AND Parent = '0'
	  AND module = @module
      UNION ALL
	-- RECURSIVE PART
      SELECT 		
		Sub.RoleKey, 
		Sub.RoleName AS NAME, Sub.RouteName, Sub.ActionName, Sub.ControllerName, Sub.Icon,  Sub.RoleID, Sub.RoleURL, Sub.Description, Sub.Slug,
		Sub.PARENT,
		DEPTH = R.DEPTH + 1, 
		SORT = R.SORT + '-' + CAST(Sub.RoleKey AS VARCHAR(MAX))
      FROM R
      INNER JOIN SYS_Role Sub ON CAST(R.RoleKey AS VARCHAR(MAX)) = Sub.Parent
	  WHERE 
	  RecordStatus <> 99
	  AND module = @module
) SELECT TREE = REPLICATE('---', R.DEPTH * 1) + R.[NAME], R.RoleKey, R.RouteName, R.ActionName, R.ControllerName, R.Icon, R.RoleID, R.RoleURL, R.Description, R.Slug FROM R WHERE 1 =1";

            if (Slug != 0)
            {
                zSQL += " AND R.Slug = @Slug";
            }
            zSQL += " ORDER BY SORT";

            string zConnectionString = ConfigurationManager.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Module", SqlDbType.NVarChar).Value = Module;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = Slug;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            List<Role_Model> zList = new List<Role_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Role_Model()
                {
                    RoleName = r["TREE"].ToString(),
                    RoleKey = r["RoleKey"].ToString(),
                    RoleID = r["RoleID"].ToString(),
                    RoleURL = r["RoleURL"].ToString(),
                    ControllerName = r["ControllerName"].ToString(),
                    ActionName = r["ActionName"].ToString(),
                    RouteName = r["RouteName"].ToString(),
                    Icon = r["Icon"].ToString(),
                    Description = r["Description"].ToString(),
                });
            }

            return zList;
        }
        public static List<Role_Model> Recursive_Role(string PartnerNumber, string Module)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"; 
WITH R AS (
	-- ANCHOR PART
      SELECT 
		  RoleKey, 
		  RoleName AS NAME, RouteName, ActionName, ControllerName, Icon, RoleID, RoleURL, Description,
		  PARENT,
		  DEPTH = 0 , 
		  SORT = CAST(RoleKey AS VARCHAR(MAX))
      FROM SYS_Role
      WHERE 
	  RecordStatus <> 99
      AND Parent = '0'
	  AND module = @module
      UNION ALL
	-- RECURSIVE PART
      SELECT 		
		Sub.RoleKey, 
		Sub.RoleName AS NAME, Sub.RouteName, Sub.ActionName, Sub.ControllerName, Sub.Icon,  Sub.RoleID, Sub.RoleURL, Sub.Description,
		Sub.PARENT,
		DEPTH = R.DEPTH + 1, 
		SORT = R.SORT + '-' + CAST(Sub.RoleKey AS VARCHAR(MAX))
      FROM R
      INNER JOIN SYS_Role Sub ON CAST(R.RoleKey AS VARCHAR(MAX)) = Sub.Parent
	  WHERE 
	  RecordStatus <> 99
	  AND module = @module
) 
SELECT TREE = REPLICATE('---',R.DEPTH*1)+R.[NAME], R.RoleKey, R.RouteName, R.ActionName, R.ControllerName, R.Icon, R.RoleID, R.RoleURL, R.Description
FROM R
WHERE R.RoleKey NOT IN (SELECT RoleKey FROM SYS_Role_Partner WHERE PartnerNumber = @PartnerNumber)
ORDER BY SORT";
            string zConnectionString = ConfigurationManager.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Module", SqlDbType.NVarChar).Value = Module;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            List<Role_Model> zList = new List<Role_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Role_Model()
                {
                    RoleName = r["TREE"].ToString(),
                    RoleKey = r["RoleKey"].ToString(),
                    RoleID = r["RoleID"].ToString(),
                    RoleURL = r["RoleURL"].ToString(),
                    ControllerName = r["ControllerName"].ToString(),
                    ActionName = r["ActionName"].ToString(),
                    RouteName = r["RouteName"].ToString(),
                    Icon = r["Icon"].ToString(),
                    Description = r["Description"].ToString(),
                });
            }

            return zList;
        }
        /// <summary>
        /// Lay bộ quyền của Partner
        /// </summary>
        /// <param name="PartnerNumber"></param>
        /// <param name="Module"></param>
        /// <returns></returns>
        public static List<Role_Partner_Model> GetPartnerRole(string PartnerNumber, string Module, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
  SELECT CONVERT(NVARCHAR(50), A.PartnerNumber) PartnerNumber, B.RoleID, B.RoleKey, B.RoleName, B.RoleURL, B.Module, 'True' AS Access, 
  B.[Level], ISNULL(B.Parent,0) AS Parent, B.[Description], B.RouteName
  FROM SYS_Role_Partner A 
  LEFT JOIN SYS_Role B ON A.RoleKey = B.RoleKey
  WHERE RecordStatus != 99 ";
            if (PartnerNumber != string.Empty)
            {
                zSQL += " AND CONVERT(NVARCHAR(50), A.PartnerNumber) = @PartnerNumber";
            }
            if (Module != string.Empty)
            {
                zSQL += " AND B.Module = @Module";
            }
            zSQL += " ORDER BY Module, RoleID";
            string zConnectionString = ConfigurationManager.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@Module", SqlDbType.NVarChar).Value = Module;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();

                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }

            List<Role_Partner_Model> zList = new List<Role_Partner_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Role_Partner_Model()
                {
                    RoleName = r["RoleName"].ToString(),
                    RoleKey = r["RoleKey"].ToString(),
                    Parent = r["Parent"].ToString(),
                    Level = r["Level"].ToInt()
                });
            }
            return zList;
        }

        public static List<Role_Partner_Model> GetPartnerRole(string PartnerNumber, string UserKey, int Slug, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
  SELECT CONVERT(NVARCHAR(50), A.PartnerNumber) PartnerNumber, B.RoleID, B.RoleKey, B.RoleName, B.RoleURL, B.Module, 'True' AS Access, 
  B.[Level], ISNULL(B.Parent,0) AS Parent, B.[Description], B.RouteName
  FROM SYS_Role_Partner A 
  LEFT JOIN SYS_Role B ON A.RoleKey = B.RoleKey
  WHERE RecordStatus != 99 AND A.RoleKey NOT IN (SELECT C.RoleKey FROM SYS_User_Role C WHERE C.UserKey = @UserKey)";
            if (PartnerNumber != string.Empty)
            {
                zSQL += " AND CONVERT(NVARCHAR(50), A.PartnerNumber) = @PartnerNumber";
            }
            if (Slug != 0)
            {
                zSQL += " AND B.Slug = @Slug";
            }
            zSQL += " ORDER BY Module, RoleID";
            string zConnectionString = ConfigurationManager.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@UserKey", SqlDbType.NVarChar).Value = UserKey;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = Slug;                
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();

                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }

            List<Role_Partner_Model> zList = new List<Role_Partner_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Role_Partner_Model()
                {
                    RoleName = r["RoleName"].ToString(),
                    RoleKey = r["RoleKey"].ToString(),
                    Parent = r["Parent"].ToString(),
                    Level = r["Level"].ToInt()
                });
            }
            return zList;
        }

        public static List<User_Role> GetUserRole(string PartnerNumber, string UserKey, int Slug, out string Message)
        {
            List<User_Role> zList = new List<User_Role>();
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT 
A.UserKey, A.RoleKey, A.RoleRead, A.RoleAdd, A.RoleEdit, A.RoleDel, 
A.RoleApprove, B.RoleName, B.RoleID, B.RoleURL, B.Module, B.Parent, 
B.[Level], B.RouteName, B.ActionName, B.ControllerName, B.ParamName, B.Icon
FROM SYS_User_Role A
LEFT JOIN SYS_Role B ON A.RoleKey = B.RoleKey
WHERE 
B.RecordStatus != 99 
AND A.UserKey = @UserKey
AND B.Slug = @Slug
ORDER BY RoleID
";

            string zConnectionString = ConfigurationManager.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@UserKey", SqlDbType.NVarChar).Value = UserKey;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = Slug;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();

                if (zTable.Rows.Count > 0)
                {
                    foreach (DataRow r in zTable.Rows)
                    {
                        zList.Add(new User_Role()
                        {
                            UserKey = r["UserKey"].ToString(),
                            RoleKey = r["RoleKey"].ToString(),
                            RoleName = r["RoleName"].ToString(),
                            RoleID = r["RoleID"].ToString(),
                            RoleURL = r["RoleURL"].ToString(),
                            Module = r["Module"].ToString(),
                            RoleRead = r["RoleRead"].ToBool(),
                            RoleAdd = r["RoleAdd"].ToBool(),
                            RoleEdit = r["RoleEdit"].ToBool(),
                            RoleDel = r["RoleDel"].ToBool(),
                            RoleExe=r["RoleApprove"].ToBool(),
                            Level = r["Level"].ToInt(),
                            Parent = r["Parent"].ToString(),
                            RouteName = r["RouteName"].ToString(),
                            ActionName = r["ActionName"].ToString(),
                            ControllerName = r["ControllerName"].ToString(),
                            ParamName = r["ParamName"].ToString(),
                            Icon = r["Icon"].ToString(),
                        });
                    }
                }

                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            return zList;
        }

        public static string ApplySQL(string SQL, out string Message)
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------

            string zConnectionString = ConfigurationManager.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(SQL, zConnect);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                Message = string.Empty;
            }
            catch (Exception Err)
            {
                Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

    }
}