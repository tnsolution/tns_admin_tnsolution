﻿using System;
namespace Admin
{
    public class Role_Model
    {
        #region [ Field Name ]
        private string _RoleKey = "";
        private string _RoleName = "";
        private string _RoleID = "";
        private string _RoleURL = "";
        private string _Description = "";
        private string _Module = "";
        private int _Category = 0;
        private int _Rank = 0;
        private int _Slug = 0;
        private int _RecordStatus = 0;
        private DateTime _CreatedOn = DateTime.MinValue;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedOn = DateTime.MinValue;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _Parent = "";
        private int _Level = 0;
        private string _RouteName = "";
        private string _ActionName = "";
        private string _ControllerName = "";
        private string _ParamName = "";
        private string _Icon = "";
        #endregion

        #region [ Properties ]
        public string RoleKey
        {
            get { return _RoleKey; }
            set { _RoleKey = value; }
        }
        public string RoleName
        {
            get { return _RoleName; }
            set { _RoleName = value; }
        }
        public string RoleID
        {
            get { return _RoleID; }
            set { _RoleID = value; }
        }
        public string RoleURL
        {
            get { return _RoleURL; }
            set { _RoleURL = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public string Module
        {
            get { return _Module; }
            set { _Module = value; }
        }
        public int Category
        {
            get { return _Category; }
            set { _Category = value; }
        }
        public int Rank
        {
            get { return _Rank; }
            set { _Rank = value; }
        }
        public int Slug
        {
            get { return _Slug; }
            set { _Slug = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public string Parent
        {
            get { return _Parent; }
            set { _Parent = value; }
        }
        public int Level
        {
            get { return _Level; }
            set { _Level = value; }
        }
        public string RouteName
        {
            get { return _RouteName; }
            set { _RouteName = value; }
        }
        public string ActionName
        {
            get { return _ActionName; }
            set { _ActionName = value; }
        }
        public string ControllerName
        {
            get { return _ControllerName; }
            set { _ControllerName = value; }
        }
        public string ParamName
        {
            get { return _ParamName; }
            set { _ParamName = value; }
        }
        public string Icon
        {
            get { return _Icon; }
            set { _Icon = value; }
        }
        #endregion
    }
}
