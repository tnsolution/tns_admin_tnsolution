﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
namespace Admin
{
    public class Role_Data
    {
        public static List<Role_Model> List(string Module, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM SYS_Role WHERE RecordStatus != 99 ";
            if (Module != string.Empty)
            {
                zSQL += " AND Module = @Module ";
            }
            zSQL += " ORDER BY Module, RoleID";
            string zConnectionString = ConfigurationManager.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Module", SqlDbType.NVarChar).Value = Module;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Role_Model> zList = new List<Role_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Role_Model()
                {
                    RoleKey = r["RoleKey"].ToString(),
                    RoleName = r["RoleName"].ToString(),
                    RoleID = r["RoleID"].ToString(),
                    RoleURL = r["RoleURL"].ToString(),
                    Description = r["Description"].ToString(),
                    Module = r["Module"].ToString(),
                    Category = r["Category"].ToInt(),
                    Rank = r["Rank"].ToInt(),
                    Slug = r["Slug"].ToInt(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                    Parent = r["Parent"].ToString(),
                    Level = r["Level"].ToInt(),
                    RouteName = r["RouteName"].ToString(),
                    ActionName = r["ActionName"].ToString(),
                    ControllerName = r["ControllerName"].ToString(),
                    ParamName = r["ParamName"].ToString(),
                    Icon = r["Icon"].ToString(),
                });
            }
            return zList;
        }
    }
}