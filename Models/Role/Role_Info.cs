﻿using System;
using System.Data;
using System.Data.SqlClient;
namespace Admin
{
    public class Role_Info
    {
        public Role_Model Role = new Role_Model();
        private string _Message = "";
        public string Code
        {
            get
            {
                if (_Message.Length >= 3)
                {
                    return _Message.Substring(0, 3);
                }
                else
                {
                    return "";
                }
            }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        #region [ Constructor Get Information ]
        public Role_Info()
        {
            Role.RoleKey = Guid.NewGuid().ToString();
        }
        public Role_Info(string RoleKey)
        {
            string zSQL = "SELECT * FROM SYS_Role WHERE RoleKey = @RoleKey AND RecordStatus != 99 ";
            string zConnectionString = ConfigurationManager.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@RoleKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(RoleKey);
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    Role.RoleKey = zReader["RoleKey"].ToString();
                    Role.RoleName = zReader["RoleName"].ToString();
                    Role.RoleID = zReader["RoleID"].ToString();
                    Role.RoleURL = zReader["RoleURL"].ToString();
                    Role.Description = zReader["Description"].ToString();
                    Role.Module = zReader["Module"].ToString();
                    if (zReader["Category"] != DBNull.Value)
                    {
                        Role.Category = int.Parse(zReader["Category"].ToString());
                    }

                    if (zReader["Rank"] != DBNull.Value)
                    {
                        Role.Rank = int.Parse(zReader["Rank"].ToString());
                    }

                    if (zReader["Slug"] != DBNull.Value)
                    {
                        Role.Slug = int.Parse(zReader["Slug"].ToString());
                    }

                    if (zReader["RecordStatus"] != DBNull.Value)
                    {
                        Role.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    }

                    if (zReader["CreatedOn"] != DBNull.Value)
                    {
                        Role.CreatedOn = (DateTime)zReader["CreatedOn"];
                    }

                    Role.CreatedBy = zReader["CreatedBy"].ToString();
                    Role.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                    {
                        Role.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    }

                    Role.ModifiedBy = zReader["ModifiedBy"].ToString();
                    Role.ModifiedName = zReader["ModifiedName"].ToString();
                    Role.Parent = zReader["Parent"].ToString();
                    if (zReader["Level"] != DBNull.Value)
                    {
                        Role.Level = int.Parse(zReader["Level"].ToString());
                    }

                    Role.RouteName = zReader["RouteName"].ToString();
                    Role.ActionName = zReader["ActionName"].ToString();
                    Role.ControllerName = zReader["ControllerName"].ToString();
                    Role.ParamName = zReader["ParamName"].ToString();
                    Role.Icon = zReader["Icon"].ToString();
                    _Message = "200 OK";
                }
                else
                {
                    _Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }

        #endregion

        #region [ Constructor Update Information ]
        public string Create_ServerKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO SYS_Role ("
         + " RoleName , RoleID , RoleURL , Description , Module , Category , Rank , Slug , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName , Parent , Level , RouteName , ActionName , ControllerName , ParamName , Icon ) "
         + " VALUES ( "
         + " @RoleName , @RoleID , @RoleURL , @Description , @Module , @Category , @Rank , @Slug , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName , @Parent , @Level , @RouteName , @ActionName , @ControllerName , @ParamName , @Icon ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@RoleName", SqlDbType.NVarChar).Value = Role.RoleName;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = Role.RoleID;
                zCommand.Parameters.Add("@RoleURL", SqlDbType.NVarChar).Value = Role.RoleURL;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Role.Description;
                zCommand.Parameters.Add("@Module", SqlDbType.NVarChar).Value = Role.Module;
                zCommand.Parameters.Add("@Category", SqlDbType.Int).Value = Role.Category;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Role.Rank;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = Role.Slug;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Role.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Role.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Role.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Role.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Role.ModifiedName;
                zCommand.Parameters.Add("@Parent", SqlDbType.NVarChar).Value = Role.Parent;
                zCommand.Parameters.Add("@Level", SqlDbType.Int).Value = Role.Level;
                zCommand.Parameters.Add("@RouteName", SqlDbType.NVarChar).Value = Role.RouteName;
                zCommand.Parameters.Add("@ActionName", SqlDbType.NVarChar).Value = Role.ActionName;
                zCommand.Parameters.Add("@ControllerName", SqlDbType.NVarChar).Value = Role.ControllerName;
                zCommand.Parameters.Add("@ParamName", SqlDbType.NVarChar).Value = Role.ParamName;
                zCommand.Parameters.Add("@Icon", SqlDbType.NVarChar).Value = Role.Icon;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Create_ClientKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO SYS_Role("
         + " RoleKey , RoleName , RoleID , RoleURL , Description , Module , Category , Rank , Slug , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName , Parent , Level , RouteName , ActionName , ControllerName , ParamName , Icon ) "
         + " VALUES ( "
         + " @RoleKey , @RoleName , @RoleID , @RoleURL , @Description , @Module , @Category , @Rank , @Slug , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName , @Parent , @Level , @RouteName , @ActionName , @ControllerName , @ParamName , @Icon ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (Role.RoleKey != "" && Role.RoleKey.Length == 36)
                {
                    zCommand.Parameters.Add("@RoleKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Role.RoleKey);
                }
                else
                {
                    zCommand.Parameters.Add("@RoleKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@RoleName", SqlDbType.NVarChar).Value = Role.RoleName;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = Role.RoleID;
                zCommand.Parameters.Add("@RoleURL", SqlDbType.NVarChar).Value = Role.RoleURL;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Role.Description;
                zCommand.Parameters.Add("@Module", SqlDbType.NVarChar).Value = Role.Module;
                zCommand.Parameters.Add("@Category", SqlDbType.Int).Value = Role.Category;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Role.Rank;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = Role.Slug;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Role.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Role.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Role.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Role.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Role.ModifiedName;
                zCommand.Parameters.Add("@Parent", SqlDbType.NVarChar).Value = Role.Parent;
                zCommand.Parameters.Add("@Level", SqlDbType.Int).Value = Role.Level;
                zCommand.Parameters.Add("@RouteName", SqlDbType.NVarChar).Value = Role.RouteName;
                zCommand.Parameters.Add("@ActionName", SqlDbType.NVarChar).Value = Role.ActionName;
                zCommand.Parameters.Add("@ControllerName", SqlDbType.NVarChar).Value = Role.ControllerName;
                zCommand.Parameters.Add("@ParamName", SqlDbType.NVarChar).Value = Role.ParamName;
                zCommand.Parameters.Add("@Icon", SqlDbType.NVarChar).Value = Role.Icon;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE SYS_Role SET "
                        + " RoleName = @RoleName,"
                        + " RoleID = @RoleID,"
                        + " RoleURL = @RoleURL,"
                        + " Description = @Description,"
                        + " Module = @Module,"
                        + " Category = @Category,"
                        + " Rank = @Rank,"
                        + " Slug = @Slug,"
                        + " RecordStatus = @RecordStatus,"
                        + " ModifiedOn = GetDate(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName,"
                        + " Parent = @Parent,"
                        + " Level = @Level,"
                        + " RouteName = @RouteName,"
                        + " ActionName = @ActionName,"
                        + " ControllerName = @ControllerName,"
                        + " ParamName = @ParamName,"
                        + " Icon = @Icon"
                        + " WHERE RoleKey = @RoleKey";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (Role.RoleKey != "" && Role.RoleKey.Length == 36)
                {
                    zCommand.Parameters.Add("@RoleKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Role.RoleKey);
                }
                else
                {
                    zCommand.Parameters.Add("@RoleKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@RoleName", SqlDbType.NVarChar).Value = Role.RoleName;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = Role.RoleID;
                zCommand.Parameters.Add("@RoleURL", SqlDbType.NVarChar).Value = Role.RoleURL;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Role.Description;
                zCommand.Parameters.Add("@Module", SqlDbType.NVarChar).Value = Role.Module;
                zCommand.Parameters.Add("@Category", SqlDbType.Int).Value = Role.Category;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Role.Rank;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = Role.Slug;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Role.RecordStatus;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Role.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Role.ModifiedName;
                zCommand.Parameters.Add("@Parent", SqlDbType.NVarChar).Value = Role.Parent;
                zCommand.Parameters.Add("@Level", SqlDbType.Int).Value = Role.Level;
                zCommand.Parameters.Add("@RouteName", SqlDbType.NVarChar).Value = Role.RouteName;
                zCommand.Parameters.Add("@ActionName", SqlDbType.NVarChar).Value = Role.ActionName;
                zCommand.Parameters.Add("@ControllerName", SqlDbType.NVarChar).Value = Role.ControllerName;
                zCommand.Parameters.Add("@ParamName", SqlDbType.NVarChar).Value = Role.ParamName;
                zCommand.Parameters.Add("@Icon", SqlDbType.NVarChar).Value = Role.Icon;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE SYS_Role SET RecordStatus = 99 WHERE RoleKey = @RoleKey";
            string zConnectionString = ConfigurationManager.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@RoleKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Role.RoleKey);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Empty()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM SYS_Role WHERE RoleKey = @RoleKey";
            string zConnectionString = ConfigurationManager.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@RoleKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Role.RoleKey);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        public string Copy()
        {
            Role.RoleKey = Guid.NewGuid().ToString();
            Role.RoleName = Role.RoleName + " - (Copy)";

            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO SYS_Role ("
         + "RoleKey, RoleName , RoleID , RoleURL , Description , Module , Category , Rank , Slug , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName , Parent , Level , RouteName , ActionName , ControllerName , ParamName , Icon ) "
         + " VALUES ( "
         + "@RoleKey, @RoleName , @RoleID , @RoleURL , @Description , @Module , @Category , @Rank , @Slug , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName , @Parent , @Level , @RouteName , @ActionName , @ControllerName , @ParamName , @Icon ) ";
            string zResult = "";
            string zConnectionString = ConfigurationManager.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@RoleKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Role.RoleKey);
                zCommand.Parameters.Add("@RoleName", SqlDbType.NVarChar).Value = Role.RoleName;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = Role.RoleID;
                zCommand.Parameters.Add("@RoleURL", SqlDbType.NVarChar).Value = Role.RoleURL;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Role.Description;
                zCommand.Parameters.Add("@Module", SqlDbType.NVarChar).Value = Role.Module;
                zCommand.Parameters.Add("@Category", SqlDbType.Int).Value = Role.Category;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Role.Rank;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = Role.Slug;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Role.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Role.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Role.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Role.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Role.ModifiedName;
                zCommand.Parameters.Add("@Parent", SqlDbType.NVarChar).Value = Role.Parent;
                zCommand.Parameters.Add("@Level", SqlDbType.Int).Value = Role.Level;
                zCommand.Parameters.Add("@RouteName", SqlDbType.NVarChar).Value = Role.RouteName;
                zCommand.Parameters.Add("@ActionName", SqlDbType.NVarChar).Value = Role.ActionName;
                zCommand.Parameters.Add("@ControllerName", SqlDbType.NVarChar).Value = Role.ControllerName;
                zCommand.Parameters.Add("@ParamName", SqlDbType.NVarChar).Value = Role.ParamName;
                zCommand.Parameters.Add("@Icon", SqlDbType.NVarChar).Value = Role.Icon;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
