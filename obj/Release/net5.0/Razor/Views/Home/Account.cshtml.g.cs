#pragma checksum "C:\_Project\_Gits\tns_admin\Views\Home\Account.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "18a75820e5d0ebb2dd9b648c0745d8941e2b0e37"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_Account), @"mvc.1.0.view", @"/Views/Home/Account.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\_Project\_Gits\tns_admin\Views\_ViewImports.cshtml"
using Admin;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\_Project\_Gits\tns_admin\Views\_ViewImports.cshtml"
using Admin.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"18a75820e5d0ebb2dd9b648c0745d8941e2b0e37", @"/Views/Home/Account.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"425c13ece3d0cfbb1fc0f84962af70815be6a0e8", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_Account : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n");
#nullable restore
#line 2 "C:\_Project\_Gits\tns_admin\Views\Home\Account.cshtml"
  
    ViewData["Title"] = "Account";
    Layout = "~/Views/Shared/_Layout.cshtml";
    var _ListPartner = new List<Partner_Model>();
    if (ViewBag.ListPartner != null)
    {
        _ListPartner = ViewBag.ListPartner as List<Partner_Model>;
    }

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
<style>
    table {
        border-collapse: collapse;
    }

    .toggle {
        display: none;
    }

        .toggle:target {
            display: table-row;
        }
</style>

<header class=""page-header"">
    <h2>Tài khoản đối tác</h2>
    <div class=""right-wrapper text-right"" style=""margin-right:10px"">
        <ol class=""breadcrumbs"">
            <li>
                <a href=""#"">
                    <i class=""fas fa-home""></i>
                </a>
            </li>
        </ol>
    </div>
</header>
<div class=""row"">
    <div class=""col-md-3"">
        <div class=""card "">
            <div class=""card-header "">

            </div>
            <div class=""card-body "">
                <table class=""table table-sm"" id=""tblPartner"">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Đối tác</th>
                            <th>.</th>
                        </tr>
                    </thead");
            WriteLiteral(">\r\n                    <tbody>\r\n");
#nullable restore
#line 54 "C:\_Project\_Gits\tns_admin\Views\Home\Account.cshtml"
                         if (_ListPartner.Count > 0)
                        {
                            for (int i = 0; i < _ListPartner.Count; i++)
                            {
                                Partner_Model rec = _ListPartner[i];


#line default
#line hidden
#nullable disable
            WriteLiteral("                                <tr");
            BeginWriteAttribute("partnernumber", " partnernumber=\"", 1612, "\"", 1646, 1);
#nullable restore
#line 60 "C:\_Project\_Gits\tns_admin\Views\Home\Account.cshtml"
WriteAttributeValue("", 1628, rec.PartnerNumber, 1628, 18, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">\r\n                                    <td>");
#nullable restore
#line 61 "C:\_Project\_Gits\tns_admin\Views\Home\Account.cshtml"
                                    Write(i + 1);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                                    <td>");
#nullable restore
#line 62 "C:\_Project\_Gits\tns_admin\Views\Home\Account.cshtml"
                                   Write(rec.StoreName);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                                    <td>.</td>\r\n                                </tr>\r\n");
#nullable restore
#line 65 "C:\_Project\_Gits\tns_admin\Views\Home\Account.cshtml"
                            }
                        }

#line default
#line hidden
#nullable disable
            WriteLiteral(@"                    </tbody>
                </table>
            </div>
            <div class=""card-footer "">
            </div>
        </div>
    </div>
    <div class=""col-md-9"">
        <div class=""card "">
            <div class=""card-header "">

            </div>
            <div class=""card-body "" id=""pnPartnerData"">

            </div>
            <div class=""card-footer "">
            </div>
        </div>
    </div>
</div>
<div id=""modalEdit"" class=""zoom-anim-dialog modal-block modal-header-color modal-block-info mfp-hide"">

</div>
<div id=""modalRole"" class=""zoom-anim-dialog modal-block modal-block-full modal-header-color modal-block-info mfp-hide"">

</div>
<input id=""txt_PartnerNumber"" type=""hidden""");
            BeginWriteAttribute("value", " value=\"", 2657, "\"", 2665, 0);
            EndWriteAttribute();
            WriteLiteral(" />\r\n");
            DefineSection("scripts", async() => {
                WriteLiteral("\r\n    <script>\r\n        var URL_Role_Remove = \"");
#nullable restore
#line 96 "C:\_Project\_Gits\tns_admin\Views\Home\Account.cshtml"
                          Write(Url.Action("Role_User_Remove", "Home"));

#line default
#line hidden
#nullable disable
                WriteLiteral("\";\r\n        var URL_Role_Remove_All = \"");
#nullable restore
#line 97 "C:\_Project\_Gits\tns_admin\Views\Home\Account.cshtml"
                              Write(Url.Action("Role_User_Remove_All", "Home"));

#line default
#line hidden
#nullable disable
                WriteLiteral("\";\r\n        var URL_Role_Add = \"");
#nullable restore
#line 98 "C:\_Project\_Gits\tns_admin\Views\Home\Account.cshtml"
                       Write(Url.Action("Role_User_Add", "Home"));

#line default
#line hidden
#nullable disable
                WriteLiteral("\";\r\n        var URL_Role_Add_All = \"");
#nullable restore
#line 99 "C:\_Project\_Gits\tns_admin\Views\Home\Account.cshtml"
                           Write(Url.Action("Role_User_Add_All", "Home"));

#line default
#line hidden
#nullable disable
                WriteLiteral("\";\r\n        var URL_Role_Update = \"");
#nullable restore
#line 100 "C:\_Project\_Gits\tns_admin\Views\Home\Account.cshtml"
                          Write(Url.Action("Role_User_Save", "Home"));

#line default
#line hidden
#nullable disable
                WriteLiteral("\";\r\n        var URL_Account_Role = \"");
#nullable restore
#line 101 "C:\_Project\_Gits\tns_admin\Views\Home\Account.cshtml"
                           Write(Url.Action("Account_Role","Home"));

#line default
#line hidden
#nullable disable
                WriteLiteral("\";\r\n        var URL_Account_Set = \"");
#nullable restore
#line 102 "C:\_Project\_Gits\tns_admin\Views\Home\Account.cshtml"
                          Write(Url.Action("Role_Account_Set", "Home"));

#line default
#line hidden
#nullable disable
                WriteLiteral("\";\r\n        var URL_Account_Default = \"");
#nullable restore
#line 103 "C:\_Project\_Gits\tns_admin\Views\Home\Account.cshtml"
                              Write(Url.Action("Role_Account_Default", "Home"));

#line default
#line hidden
#nullable disable
                WriteLiteral("\";\r\n\r\n        var URL_Account = \"");
#nullable restore
#line 105 "C:\_Project\_Gits\tns_admin\Views\Home\Account.cshtml"
                      Write(Url.Action("Account_Partner","Home"));

#line default
#line hidden
#nullable disable
                WriteLiteral("\";\r\n        var URL_Check = \"");
#nullable restore
#line 106 "C:\_Project\_Gits\tns_admin\Views\Home\Account.cshtml"
                    Write(Url.Action("Check_Account","Home"));

#line default
#line hidden
#nullable disable
                WriteLiteral("\";\r\n        var URL_Edit = \"");
#nullable restore
#line 107 "C:\_Project\_Gits\tns_admin\Views\Home\Account.cshtml"
                   Write(Url.Action("Edit_Account", "Home"));

#line default
#line hidden
#nullable disable
                WriteLiteral("\";\r\n        var URL_Delete = \"");
#nullable restore
#line 108 "C:\_Project\_Gits\tns_admin\Views\Home\Account.cshtml"
                     Write(Url.Action("Delete_Account", "Home"));

#line default
#line hidden
#nullable disable
                WriteLiteral("\";\r\n        var URL_Save = \"");
#nullable restore
#line 109 "C:\_Project\_Gits\tns_admin\Views\Home\Account.cshtml"
                   Write(Url.Action("Save_Account", "Home"));

#line default
#line hidden
#nullable disable
                WriteLiteral("\";\r\n        var URL_Reset = \"");
#nullable restore
#line 110 "C:\_Project\_Gits\tns_admin\Views\Home\Account.cshtml"
                    Write(Url.Action("Reset_Pass", "Home"));

#line default
#line hidden
#nullable disable
                WriteLiteral("\";\r\n        var URL_Active = \"");
#nullable restore
#line 111 "C:\_Project\_Gits\tns_admin\Views\Home\Account.cshtml"
                     Write(Url.Action("Activate", "Home"));

#line default
#line hidden
#nullable disable
                WriteLiteral(@""";
    </script>
    <script>
        var width = $(window).width();
        const userAgent = navigator.userAgent.toLowerCase();
        const isTablet = /(ipad|tablet|(android(?!.*mobile))|(windows(?!.*phone)(.*touch))|kindle|playbook|silk|(puffin(?!.*(IP|AP|WP))))/.test(userAgent);

        $(document).ready(function () {
            if (isTablet || width <= 1366) {
                $(""html"").addClass(""sidebar-left-collapsed"");
            }
            $(""[name=\""divTable\""]"").css({
                ""height"": $(window).height() - 390,
            });

            $(""#tblPartner"").on(""click"", ""tr"", function (e) {
                var partner = $(this).attr(""partnernumber"");
                $(""#txt_PartnerNumber"").val(partner);

                e.preventDefault();
                // removes all highlights from tr's
                $(""#tblPartner tr"").removeClass(""highlight"");
                // adds the highlight to this row
                $(this).addClass(""highlight"");
               ");
                WriteLiteral(@" GetPartnerData(partner);
            });

            var param = Utils.GetUrlParameter(""PartnerNumber"");
            if (param.length > 0) {
                $(""#tblPartner tr[partnernumber=\"""" + param + ""\""]"").addClass(""highlight"");
                GetPartnerData(param);
            }
        });
       
        function InitCheckStyle() {
            $(""[data-plugin-ios-switch]"").each(function () {
                var $this = $(this);
                $this.themePluginIOS7Switch();
            });
        }
        function GetPartnerData(partner) {
            $.ajax({
                url: URL_Account,
                type: ""GET"",
                data: {
                    ""PartnerNumber"": partner,
                },
                beforeSend: function () {
                    $(""#pnPartnerData"").empty();
                },
                success: function (r) {
                    $(""#pnPartnerData"").html(r);
                },
                error: function (err) {
     ");
                WriteLiteral(@"               Utils.OpenNotify(""Lỗi !."", err.responseText, ""error"");
                },
                complete: function () {
                    InitCheckStyle();
                }
            });
        }
        function Edit(userkey) {
            $.ajax({
                url: URL_Edit,
                type: ""GET"",
                data: {
                    ""UserKey"": userkey
                },
                beforeSend: function () {
                    Utils.LoadIn();
                    Utils.OpenMagnific(""#modalEdit"");
                    $(""#modalEdit"").empty();
                },
                success: function (r) {
                    $(""#modalEdit"").html(r);
                },
                error: function (err) {
                    Utils.OpenNotify(err.responseText, ""Thông báo"", ""error"");
                },
                complete: function () {
                    Utils.LoadOut();
                    if (userkey.length < 36) {
                        Util");
                WriteLiteral(@"s.ClearUI(""#modalEdit"");
                        $(""#txt_ExpireDate"").val(Utils.GetCurrentDate());
                    }
                    $("".select2"").select2({
                        width: ""100%"",
                        placeholder: ""--Chọn--"",
                    });
                    $(""#txt_UserName"").on(""blur"", function () {
                        var str = $(this).val();
                        if (str.length > 0) {
                            CheckUser(str);
                        }
                    });
                }
            });
        }
        function Delete(userkey) {
            $.confirm({
                type: ""red"",
                typeAnimated: true,
                title: ""Cảnh báo !."",
                content: ""Bạn có chắc xóa thông tin này ?."",
                buttons: {
                    confirm: {
                        text: ""Đồng ý"",
                        btnClass: ""btn-red"",
                        action: function () {
           ");
                WriteLiteral(@"                 $.ajax({
                                url: URL_Delete,
                                type: ""POST"",
                                data: {
                                    ""UserKey"": userkey
                                },
                                beforeSend: function () {

                                },
                                success: function (r) {
                                    if (r.success) {
                                        $(""tr[userkey=\"""" + userkey + ""\""]"")
                                            .addClass(""on-remove"")
                                            .fadeOut(function () {
                                                $(this).remove();
                                            });
                                    }
                                    else {
                                        Utils.OpenNotify(""Lỗi !."", r.message, ""error"");
                                    }
                  ");
                WriteLiteral(@"              },
                                error: function (err) {
                                    Utils.OpenNotify(""Lỗi !."", err.responseText, ""error"");
                                },
                                complete: function () {

                                }
                            });
                        }
                    },
                    cancel: {
                        text: ""Hủy"",
                    }
                }
            });
        }
        //kich hoạt
        function Activate(userkey) {
            $.ajax({
                url: URL_Active,
                type: ""POST"",
                data: {
                    ""UserKey"": userkey,
                },
                beforeSend: function () {

                },
                success: function (r) {
                    if (r.success) {
                        Utils.OpenNotify(""Cập nhật tình trạng thành công"", ""Thông báo"", ""success"");
                    }
    ");
                WriteLiteral(@"                else {
                        Utils.OpenNotify(r.message, ""Thông báo"", ""error"");
                    }
                },
                error: function (err) {
                    Utils.OpenNotify(err.responseText, ""Thông báo"", ""error"");
                },
                complete: function () {

                }
            });
        }
        //reset mat khau
        function Reset(userkey) {
            $.ajax({
                url: URL_Reset,
                type: ""POST"",
                data: {
                    ""UserKey"": userkey,
                },
                beforeSend: function () {

                },
                success: function (r) {
                    if (r.success) {
                        Utils.OpenNotify(r.message, ""Tạo mật khẩu mới thành công"", ""success"");
                    }
                    else {
                        Utils.OpenNotify(r.message, ""Thông báo"", ""error"");
                    }
                },
        ");
                WriteLiteral(@"        error: function (err) {
                    Utils.OpenNotify(err.responseText, ""Thông báo"", ""error"");
                },
                complete: function () {

                }
            });
        }
        //kiem tra tên user
        function CheckUser(name) {
            var exist = false;

            var val = """";
            if (name === undefined ||
                name === """") {
                val = $(""#txt_UserName"").val();
            }
            else {
                val = name;
            }

            $.ajax({
                async: false,
                url: URL_Check,
                type: ""GET"",
                data: {
                    ""UserName"": val,
                },
                beforeSend: function () {

                },
                success: function (r) {
                    if (!r.success) {
                        Utils.OpenNotify(""Thông báo"", r.message, ""error"");
                        exist = false;
               ");
                WriteLiteral(@"     }
                    else {                      
                        exist = true;
                    }                    
                },
                error: function (err) {
                    Utils.OpenNotify(""Thông báo"", err.responseText, ""error"");
                },
                complete: function () {
                  
                }
            });

            return exist;
        }

        //load modal role
        function SetRoleDesktop(partner, userkey) {

            $.ajax({
                url: URL_Account_Role,
                type: ""GET"",
                data: {
                    ""PartnerNumber"": partner,
                    ""UserKey"": userkey,
                    ""Slug"": 8,
                },
                beforeSend: function () {
                    Utils.LoadIn();
                    Utils.OpenMagnific(""#modalRole"");
                    $(""#modalRole"").empty();
                },
                success: function (r) {
    ");
                WriteLiteral(@"                $(""#modalRole"").html(r);
                },
                error: function (err) {
                    Utils.OpenNotify(""Thông báo"", err.responseText, ""error"");
                },
                complete: function () {
                    Utils.LoadOut();
                    ModalRoleEvent();
                    $(""#txt_Slug"").val(8);
                }
            });
        }
        function SetRoleMobile(partner, userkey) {

            $.ajax({
                url: URL_Account_Role,
                type: ""GET"",
                data: {
                    ""PartnerNumber"": partner,
                    ""UserKey"": userkey,
                    ""Slug"": 9
                },
                beforeSend: function () {
                    Utils.LoadIn();
                    Utils.OpenMagnific(""#modalRole"");
                    $(""#modalRole"").empty();
                },
                success: function (r) {
                    $(""#modalRole"").html(r);
               ");
                WriteLiteral(@" },
                error: function (err) {
                    Utils.OpenNotify(""Thông báo"", err.responseText, ""error"");
                },
                complete: function () {
                    Utils.LoadOut();
                    ModalRoleEvent();
                    $(""#txt_Slug"").val(9);
                }
            });
        }

        function ModalRoleEvent() {
            $(""#dchkRead_all"").click(function (e) {
                $(this).closest('table').find('td input[name=""chkRead"" ]:checkbox').prop('checked', this.checked);
                console.log('chkall');
            });
            $(""#dchkAdd_all"").click(function (e) {
                $(this).closest('table').find('td input[name=""chkAdd"" ]:checkbox').prop('checked', this.checked);
                console.log('chkadd');
            });
            $(""#dchkEdit_all"").click(function (e) {
                $(this).closest('table').find('td input[name=""chkEdit"" ]:checkbox').prop('checked', this.checked);
            ");
                WriteLiteral(@"    console.log('chkedit');
            });
            $(""#dchkDel_all"").click(function (e) {
                $(this).closest('table').find('td input[name=""chkDel"" ]:checkbox').prop('checked', this.checked);
                console.log('chkdel');
            });
            $(""#dchkExe_all"").click(function (e) {
                $(this).closest('table').find('td input[name=""chkExe"" ]:checkbox').prop('checked', this.checked);
                console.log('chkexe');
            });

            $(""[name=\""divTable\""]"").css({
                ""height"": $(window).height() - 490,
            });
            $("".tblFix"").tableHeadFixer({
                head: true,
                foot: false,
                left: 0,
                right: 0,
                ""z-index"": ""501""
            });

            //new array;
            roles = [];
        }

        function RemoveRoleAll() {
            var UserKey = $(""#txt_UserKey"").val();
            var PartnerNumber = $(""#txt_PartnerNumber""");
                WriteLiteral(@").val();
            var Slug = $(""#txt_Slug"").val();

            $.confirm({
                type: ""red"",
                typeAnimated: true,
                title: ""Cảnh báo !."",
                content: ""Bạn có chắc xóa hết tất cả quyền này ?."",
                buttons: {
                    confirm: {
                        text: ""Đồng ý"",
                        btnClass: ""btn-red"",
                        action: function () {

                            $.ajax({
                                url: URL_Role_Remove_All,
                                type: ""POST"",
                                data: {
                                    ""UserKey"": UserKey,
                                },
                                beforeSend: function () {
                                    Utils.LoadIn();
                                },
                                success: function (r) {
                                    if (r.success) {
                                 ");
                WriteLiteral(@"       GetAccountRole(UserKey, PartnerNumber, Slug);
                                        GetAccountDefault(UserKey, PartnerNumber, Slug);
                                    }
                                },
                                error: function (err) {
                                    Utils.OpenNotify(""Lỗi !."", err.responseText, ""error"");
                                },
                                complete: function () {
                                    Utils.LoadOut();
                                }
                            });
                        }
                    },
                    cancel: {
                        text: ""Hủy"",
                    }
                }
            });

        }
        function RemoveRole(rolekey) {
            var $tr = $(""tr[rolekey=\"""" + rolekey + ""\""]"");
            var UserKey = $(""#txt_UserKey"").val();
            var PartnerNumber = $(""#txt_PartnerNumber"").val();
            var Slug = $(""#txt_S");
                WriteLiteral(@"lug"").val();

            $.ajax({
                url: URL_Role_Remove,
                type: ""POST"",
                data: {
                    ""RoleKey"": rolekey,
                    ""UserKey"": UserKey,
                },
                beforeSend: function () {
                    Utils.LoadIn();
                },
                success: function (r) {
                    if (r.success) {
                        //reload modal
                        $tr.remove();
                        GetAccountDefault(UserKey, PartnerNumber, Slug);
                    }
                },
                error: function (err) {
                    Utils.OpenNotify(""Lỗi !."", err.responseText, ""error"");
                },
                complete: function () {
                    Utils.LoadOut();
                }
            });
        }
        function AddAll() {
            var ListRole = [];
            var UserKey = $(""#txt_UserKey"").val();
            var PartnerNumber = $(""#tx");
                WriteLiteral(@"t_PartnerNumber"").val();
            var Slug = $(""#txt_Slug"").val();

            $(""#tblRole tbody tr"").each(function () {

                var obj = {
                    RoleKey: $(this).attr(""rolekey""),
                    UserKey: UserKey,
                    PartnerNumber: PartnerNumber,
                };
                ListRole.push(obj);
            });

            console.log(ListRole);

            $.ajax({
                url: URL_Role_Add_All,
                type: ""POST"",
                data: {
                    ""ListRole"": ListRole
                },
                beforeSend: function () {
                    Utils.LoadIn();
                },
                success: function (r) {
                    if (r.success) {
                        //reload modal
                        GetAccountRole(UserKey, PartnerNumber, Slug);
                        GetAccountDefault(UserKey, PartnerNumber, Slug);
                    }
                },
                er");
                WriteLiteral(@"ror: function (err) {
                    Utils.OpenNotify(""Lỗi !."", err.responseText, ""error"");
                },
                complete: function () {
                    Utils.LoadOut();
                }
            });
        }
        function AddRole(rolekey) {
            var $tr = $(""tr[rolekey=\"""" + rolekey + ""\""]"");
            var UserKey = $(""#txt_UserKey"").val();
            var PartnerNumber = $(""#txt_PartnerNumber"").val();
            var Slug = $(""#txt_Slug"").val();
            $.ajax({
                url: URL_Role_Add,
                type: ""POST"",
                data: {
                    ""RoleKey"": rolekey,
                    ""UserKey"": UserKey,
                    ""Slug"": Slug
                },
                beforeSend: function () {
                    Utils.LoadIn();
                },
                success: function (r) {
                    if (r.success) {
                        //reload modal
                        $tr.remove();
           ");
                WriteLiteral(@"             GetAccountRole(UserKey, PartnerNumber, Slug);
                    }
                },
                error: function (err) {
                    Utils.OpenNotify(""Lỗi !."", err.responseText, ""error"");
                },
                complete: function () {
                    Utils.LoadOut();
                }
            });
        }

        var roles = [];
        function SaveRole() {
            $(""#tblRoleUser > tbody > tr"").each(function () {
                var rolekey = $(this).attr(""rolekey"");
                var userkey = $(this).attr(""userkey"");
                SetRole(this, rolekey, userkey);
            });

            var UserKey = $(""#txt_UserKey"").val();
            var Slug = $(""#txt_Slug"").val();
            var Role = $(""#txt_UserRole"").val();

            console.log(Role);

            $.ajax({
                url: URL_Role_Update,
                type: ""POST"",
                data: {
                    ""UserKey"": UserKey,
              ");
                WriteLiteral(@"      ""Slug"": Slug,
                    ""Role"": Role,
                },
                beforeSend: function () {

                },
                success: function (r) {
                    if (!r.success) {
                        Utils.OpenNotify(""Thông báo"", r.message, ""error"");
                    }
                    else {
                        Utils.CloseMagnific(""#modalRole"");
                    }
                },
                error: function (err) {
                    Utils.OpenNotify(""Thông báo"", err.responseText, ""error"");
                },
                complete: function () {

                }
            });
        }

        function SetRole(obj, rolekey, userkey) {
            var chk = $(obj).find('input:checkbox');
            var read = chk[0].checked;
            var add = chk[1].checked;
            var edit = chk[2].checked;
            var del = chk[3].checked;
            var exe = chk[4].checked;

            var newobj = {
         ");
                WriteLiteral(@"       ""RoleKey"": rolekey,
                ""UserKey"": userkey,
                ""RoleRead"": read,
                ""RoleAdd"": add,
                ""RoleEdit"": edit,
                ""RoleDel"": del,
                ""RoleExe"": exe,
            };

            roles = ObjCheck(roles, ""RoleKey"", newobj);
            $(""#txt_UserRole"").val(JSON.stringify(roles));
        }
        function ObjCheck(array, name, newElement) {
            var found = false;
            for (var i = 0; i < array.length; i++) {
                var element = array[i];
                if (element === undefined) {
                    array.push(newElement);
                    return array;
                }
                if (element[name].toString() ===
                    newElement[name].toString()) {
                    found = true;
                    array[i] = newElement;
                }
            }
            if (found === false) {
                array.push(newElement);
            }

         ");
                WriteLiteral(@"   return array;

        }

        function GetAccountRole(userkey, partner, slug) {
            $.ajax({
                url: URL_Account_Set,
                type: ""GET"",
                data: {
                    ""PartnerNumber"": partner,
                    ""UserKey"": userkey,
                    ""Slug"": slug
                },
                beforeSend: function () {
                    $(""#pnAccountRole"").empty();
                },
                success: function (r) {
                    $(""#pnAccountRole"").html(r);
                },
                error: function (err) {
                    Utils.OpenNotify(""Lỗi !."", err.responseText, ""error"");
                },
                complete: function () {
                    ModalRoleEvent();
                }
            });
        }

        function GetAccountDefault(userkey, partner, slug) {
            $.ajax({
                url: URL_Account_Default,
                type: ""GET"",
                data: {
  ");
                WriteLiteral(@"                  ""PartnerNumber"": partner,
                    ""UserKey"": userkey,
                    ""Slug"": slug
                },
                beforeSend: function () {
                    $(""#pnAccountDefault"").empty();
                },
                success: function (r) {
                    $(""#pnAccountDefault"").html(r);
                },
                error: function (err) {
                    Utils.OpenNotify(""Lỗi !."", err.responseText, ""error"");
                },
                complete: function () {
                    ModalRoleEvent();
                }
            });
        }
            //


    </script>
");
            }
            );
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
